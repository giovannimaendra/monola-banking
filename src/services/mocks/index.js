import * as petHandlers from './pets/petsHandlers';
import * as accessoriesHandlers from './accessories/accessoriesHandlers';

const handlers = { ...petHandlers, ...accessoriesHandlers };

export { handlers };
