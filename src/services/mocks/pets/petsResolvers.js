import petsData from './petsData';

const getAllPets = (req, res, ctx) => {
  return res(
    ctx.status(200),
    ctx.json({
      type: 'Success',
      message: 'Get data success',
      content: petsData,
    })
  );
};

export { getAllPets };
