import Reindeer from '../../../assets/img/Reindeer.svg';
import Predator from '../../../assets/img/Predator.svg';
import SleepWear from '../../../assets/img/SleepWear.svg';
import Gentleman from '../../../assets/img/Gentleman.svg';

const accessoriesData = [
  {
    id: 1,
    accessoriesName: 'Reindeer',
    accessoriesImg: Reindeer,
    accessoriesPrice: 200,
    accessoriesAvailability: ['Hamster'],
  },
  {
    id: 2,
    accessoriesName: 'Child Predator',
    accessoriesImg: Predator,
    accessoriesPrice: 200,
    accessoriesAvailability: ['Hamster'],
  },
  {
    id: 3,
    accessoriesName: 'Sleep Wear',
    accessoriesImg: SleepWear,
    accessoriesPrice: 200,
    accessoriesAvailability: ['Hamster'],
  },
  {
    id: 4,
    accessoriesName: 'Gentleman',
    accessoriesImg: Gentleman,
    accessoriesPrice: 200,
    accessoriesAvailability: ['Hamster'],
  },
];

export default accessoriesData;
