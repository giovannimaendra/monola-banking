import { serviceWorker } from 'msw';
import { handlers } from '.';

export const worker = serviceWorker(...handlers);
