import getVendorComponent from './getVendorComponent';
import getSavingIconComponent from './getSavingIconComponent';
import renderSavingIcon from './renderSavingIcon';
import * as localStorageHelpers from './localStorageHelpers';

const classNameJoin = (classArr) => {
  return classArr.join(' ').trim();
};

const separatorWithDot = (text) => {
  if (text) {
    return text.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  return '-';
};

export const replaceSpaceAndLowerCase = (text) => {
  return text.replace(/\s+/g, '-').toLowerCase();
};

const creditCardNumberFormat = (ccNumberString) => {
  // return ccNumberString.toString().replace(/(\d{4}(?!\s))/g, '$1 ');
  if (ccNumberString) {
    return ccNumberString.toString().replace(/\B(?=(\d{4})+(?!\d))/g, ' ');
  }
};

const checkObjectIsNotEmpty = (obj) => {
  return !!Object.keys(obj).length;
};

const formatObjectData = (id, userName, provider, identityNumber) => {
  const formattedObject = {
    id,
    userName,
    provider,
    identityNumber,
  };

  return formattedObject;
};

const formatItemObjectData = (id, itemName, itemImg, itemPrice) => {
  const formattedObject = {
    id,
    itemName,
    itemImg,
    itemPrice,
  };

  return formattedObject;
};

const limitData = (data, limit) => {
  const limitedData = [];
  for (let i = data.length - 1; limitedData.length < limit && data[i]; i--) {
    limitedData.push(data[i]);
  }

  return limitedData;
};

const isValidEmail = (email) => {
  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email)) {
    return false;
  }
  return true;
};

const isValidPassword = (password) => {
  if (password.length < 6) {
    return false;
  }
  return true;
};

export {
  getVendorComponent,
  getSavingIconComponent,
  renderSavingIcon,
  localStorageHelpers,
  classNameJoin,
  separatorWithDot,
  creditCardNumberFormat,
  checkObjectIsNotEmpty,
  formatObjectData,
  limitData,
  formatItemObjectData,
  isValidEmail,
  isValidPassword,
};
