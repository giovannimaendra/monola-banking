import { ReactComponent as Ambulance } from '../../assets/img/ambulance.svg';
import { ReactComponent as Plane } from '../../assets/img/airplane.svg';
import { ReactComponent as Car } from '../../assets/img/car.svg';
import { ReactComponent as GradCap } from '../../assets/img/gradcap.svg';
import { ReactComponent as Camera } from '../../assets/img/camera.svg';
import { ReactComponent as Paint } from '../../assets/img/paint.svg';

const getSavingIconComponent = (iconName) => {
  const icons = {
    Ambulance,
    Plane,
    Car,
    GradCap,
    Camera,
    Paint
  }

  return icons[iconName];
}

export default getSavingIconComponent