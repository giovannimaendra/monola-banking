import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import App from './app/App';
import { store } from './app/store';
import './index.scss';

const wrappedComponent = (
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>
);

ReactDOM.render(wrappedComponent, document.querySelector('#root'));
