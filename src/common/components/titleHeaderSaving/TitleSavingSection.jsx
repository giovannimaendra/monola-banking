import styles from './TitleSavingSection.module.scss';
import { BiLeftArrowAlt } from "react-icons/bi";
import Coin from '../../../assets/img/coin.svg';
import { NavLink } from 'react-router-dom';

const TitleSavingSection = ({ onClick, title, goToBack, coin }) => {
    return (
        <div className={styles.addNewSavingTitle}>
                <NavLink className={styles.navLink} to="/dashboard/saving">
                <BiLeftArrowAlt size="20px"
                className={styles.iconBack}
                viewBox="0 0 20 20"
                onClick={onClick}/>
                </NavLink>
                <h5 className={styles.title}>{title}</h5>
                {(coin ? 
                <div className={styles.coinContainer}>
                    <img className={styles.coin} src={Coin} alt="" />
                    <span className={styles.coinText}>{coin ? coin : ''}</span>
                </div>
                : '')}
        </div>
    )
}

export default TitleSavingSection
