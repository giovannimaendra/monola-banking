const DashboardLayoutHead = ({ children }) => {
  return (
    <>
      <header className="d-flex justify-content-between align-items-center">
        {/* <h3 className={styles.headingText}>{children}</h3> */}
        {children}
      </header>
      <hr />
    </>
  );
};

export default DashboardLayoutHead;
