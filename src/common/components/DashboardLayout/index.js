import DashboardLayout from './DashboardLayout';
import DashboardLayoutHead from './DashboardLayoutHead';
import DashboardLayoutBody from './DashboardLayoutBody';

DashboardLayout.Head = DashboardLayoutHead;
DashboardLayout.Body = DashboardLayoutBody;

export { DashboardLayout };
