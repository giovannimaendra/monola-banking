import { Button } from 'react-bootstrap';

import { classNameJoin } from '../../../services/helpers';

import styles from './Carousel.module.scss';

const Carousel = ({
  bgImg,
  title,
  subTitle,
  contentImg,
  className,
  id,
  onClick,
}) => {
  const carouselItemClassName = classNameJoin([
    'carousel-item overflow-hidden',
    styles.carouselItem,
    className,
  ]);

  const carouselTextClassName = classNameJoin([
    id === 3 ? 'col-sm-9 px-0' : 'col-sm-8 px-0',
    styles.carouselItemText,
  ]);

  const carouselRowClassName = classNameJoin([
    'row w-100',
    styles.carouselItemRow,
  ]);

  const carouselImgClassName = classNameJoin([
    id === 3
      ? 'col-sm-3 px-0 text-left text-md-center'
      : 'col-sm-4 px-0 text-md-center',
    styles.carouselImgContainer,
  ]);

  const carouselImgItemClassName = classNameJoin([
    id === 3 ? styles.coin : styles.imgCarousel,
  ]);

  return (
    <div className={carouselItemClassName}>
      <img src={bgImg} alt="Background" className={styles.bgImg} />
      <div className="container-fluid d-flex align-items-center h-100 justify-content-center">
        <div className={carouselRowClassName}>
          <div className={carouselTextClassName}>
            <div className={styles.carouselTextContainer}>
              <h3 className={styles.titleCarousel}>{title}</h3>
              <p className={styles.subTitleCarousel}>{subTitle}</p>
              <Button className={styles.btnRegis} onClick={onClick}>
                Daftar Sekarang
              </Button>
            </div>
          </div>
          <div className={carouselImgClassName}>
            <img
              src={contentImg}
              className={carouselImgItemClassName}
              alt="Activate account"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Carousel;
