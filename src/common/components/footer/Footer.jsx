import { FaFacebookF, FaTwitter, FaInstagram, FaYoutube } from 'react-icons/fa';
import { Link } from 'react-router-dom';

import { classNameJoin } from '../../../services/helpers';

import Monola from '../monola/Monola';

import GooglePlay from '../../../assets/img/GooglePlay.png';
import WebBrowser from '../../../assets/img/WebBrowser.png';
import styles from './Footer.module.scss';

const Footer = ({ isHidden }) => {
  const socialMedias = [
    {
      id: 1,
      platform: 'facebook',
      link: 'http://www.facebook.com',
      component: <FaFacebookF className={styles.icon} />,
    },
    {
      id: 2,
      platform: 'twitter',
      link: 'http://www.twitter.com',
      component: <FaTwitter className={styles.icon} />,
    },
    {
      id: 3,
      platform: 'instagram',
      link: 'http://www.instagram.com',
      component: <FaInstagram className={styles.icon} />,
    },
    {
      id: 4,
      platform: 'youtube',
      link: 'http://www.youtube.com',
      component: <FaYoutube className={styles.icon} />,
    },
  ];

  const iconContainerClassName = classNameJoin([
    'col-3 d-flex justify-content-center',
    styles.iconContainer,
  ]);

  const renderedIcons = socialMedias.map(({ link, component, id }) => (
    <div className={iconContainerClassName} key={id}>
      <a
        href={link}
        className={`${styles.footerIcon}`}
        target="_blank"
        rel="noreferrer">
        {component}
      </a>
    </div>
  ));

  const imgContainerClassName = classNameJoin([
    'col-6 px-0',
    styles.imgContainer,
  ]);

  const footerHeadingClassName = classNameJoin([
    'footer-heading',
    styles.getMonola,
  ]);

  const divKebijakanClassName = classNameJoin([
    'row justify-content-center',
    styles.kebijakan,
  ]);

  const footerLogoClassName = classNameJoin([
    'row container justify-content-evenly align-items-center',
    styles.footerLogo,
  ]);

  const footerIconsContainerClassName = classNameJoin([
    'col-md-4 d-flex flex-column justify-content-end justify-content-md-center',
    styles.footerIconsContainer,
  ]);

  const monolaLogoClassName = classNameJoin([
    'col-xl-3 col-lg-4 col-md-5 col-sm-4 px-0',
    styles.monolaLogo,
  ]);

  const monolaWarrantyClassName = classNameJoin([
    'col-xl-9 col-lg-8 col-md-7 col-sm-8 px-0 ',
    styles.monolaWarranty,
  ]);

  const mainRowClassName = classNameJoin([
    'row mx-auto px-md-0 u-margin-y-lg-large position-relative',
    styles.mainRow,
  ]);

  return (
    <footer
      className={`${styles.footer} ${!isHidden ? 'u-offset-height' : ''}`}>
      <div className={mainRowClassName}>
        <div className="col-md-8 d-flex flex-column justify-content-between">
          <div className="row container">
            <h2 className={footerHeadingClassName}>Get Monola App</h2>
          </div>
          <div className="row container pl-2 my-3">
            <a
              href="https://play.google.com/store"
              target="_blank"
              rel="noreferrer"
              className={imgContainerClassName}>
              <img
                src={GooglePlay}
                alt="Google Play"
                className={styles.vendorImg}
              />
            </a>
            <div
              className={imgContainerClassName}
              onClick={() => window.scrollTo(0, 0)}>
              <img
                src={WebBrowser}
                alt="Web App"
                className={styles.vendorImg}
              />
            </div>
          </div>
          <div className={footerLogoClassName}>
            <div className={monolaLogoClassName}>
              <Monola size="lg" color="primary" />
            </div>
            <div className={monolaWarrantyClassName}>
              <p className={styles.footerParagraph}>
                PT Monola Tbk terdaftar dan diawasi oleh Otoritas Jasa Keuangan
                (OJK) serta dijamin oleh Lembaga Penjamin Simpanan (LPS).
              </p>
            </div>
          </div>
        </div>
        <div className={footerIconsContainerClassName}>
          <div className="row mb-3">
            <div className="col mt-md-5">
              <div className="row justify-content-center no-gutters">
                {renderedIcons}
              </div>
            </div>
          </div>
          <div className={divKebijakanClassName}>
            <div className="col-md-5 col-lg-5 col-xl-4 px-0">
              <Link
                to="/privacy-policies"
                className={`${styles.footerParagraphSmall} mb-0`}>
                Kebijakan Privasi
              </Link>
            </div>
            <div className="col-md-5 col-lg-5 col-xl-4 px-0">
              <Link
                to="/terms-and-conditions"
                className={`${styles.footerParagraphSmall} mb-0`}>
                Syarat & Ketentuan
              </Link>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
