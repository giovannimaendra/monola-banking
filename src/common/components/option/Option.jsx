import { useLocation } from 'react-router-dom';

import Ewallet from '../../../assets/img/option-ewallet.svg';
import Mobile from '../../../assets/img/option-mobile.svg';
import Listrik from '../../../assets/img/option-listrik.svg';
import Pdam from '../../../assets/img/option-pdam.svg';
import ECommerce from '../../../assets/img/option-ecommerce.svg';
import Lainnya from '../../../assets/img/option-lainnya.svg';
import Asuransi from '../../../assets/img/Asuransi.svg';
import KartuKredit from '../../../assets/img/KartuKredit.svg';
import Tiket from '../../../assets/img/Tiket.svg';
import TV from '../../../assets/img/TV.svg';
import Games from '../../../assets/img/Games.svg';

import OptionItem from './OptionItem';

const Option = () => {
  const location = useLocation();

  const OPTION_DATA = [
    {
      id: 1,
      name: 'e-Wallet',
      optionImg: Ewallet,
    },
    {
      id: 2,
      name: 'Mobile & Data',
      optionImg: Mobile,
    },
    {
      id: 3,
      name: 'Listrik',
      optionImg: Listrik,
    },
    {
      id: 4,
      name: 'PDAM',
      optionImg: Pdam,
    },
    {
      id: 5,
      name: 'e-Commerce',
      optionImg: ECommerce,
    },
    {
      id: 6,
      name: 'Lainnya',
      optionImg: Lainnya,
    },
    {
      id: 7,
      name: 'Asuransi',
      optionImg: Asuransi,
    },
    {
      id: 8,
      name: 'KartuKredit',
      optionImg: KartuKredit,
    },
    {
      id: 9,
      name: 'Tiket',
      optionImg: Tiket,
    },
    {
      id: 10,
      name: 'TV',
      optionImg: TV,
    },
    {
      id: 11,
      name: 'Games',
      optionImg: Games,
    },
  ];

  const renderedOption = OPTION_DATA.map((option) => {
    if (!location.pathname.includes('transaksi')) {
      return (
        option.id < 7 && (
          <OptionItem
            key={option.id}
            name={option.name}
            image={option.optionImg}
            id={option.id}
          />
        )
      );
    }

    return (
      option.id !== 6 && (
        <OptionItem
          key={option.id}
          name={option.name}
          image={option.optionImg}
          id={option.id}
        />
      )
    );
  });

  return (
    <div className="d-flex justify-content-between my-4 flex-wrap pr-3">
      {renderedOption}
    </div>
  );
};

export default Option;
