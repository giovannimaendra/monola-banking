import { Link, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { eWalletActions } from '../../../features/ewallet';

import styles from './OptionItem.module.scss';

const OptionItem = ({ name, image, id }) => {
  const dispatch = useDispatch();
  const location = useLocation();

  return (
    <div className={`col ${location.pathname.includes('transfer') && 'ml-3'}`}>
      <div
        className={`${styles.imageContainer} ${id === 1 && styles.activeItem}`}>
        {id === 1 ? (
          <Link
            to="/dashboard/ewallet/top-up-ewallet"
            onClick={() =>
              dispatch(eWalletActions.resetFavoriteEWalletFetchStatus())
            }>
            <img className="align-self-center" src={image} alt="option" />
          </Link>
        ) : (
          <img className="text-center" src={image} alt="option" />
        )}
      </div>
      <p className={styles.name}>{name}</p>
    </div>
  );
};

export default OptionItem;
