import { Button } from 'react-bootstrap';
import { classNameJoin } from '../../../services/helpers';
import styles from './ButtonShop.module.scss';

const ButtonShop = ({children, onClick}) => {
    const buttonCN = classNameJoin([styles.button]);
    return (
        <Button className={buttonCN}>{children}</Button>
    )
}

export default ButtonShop
