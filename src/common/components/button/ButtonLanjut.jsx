import styles from './ButtonLanjut.module.scss';
import { Button } from 'react-bootstrap';

const ButtonLanjut = ({title, disabled, onClick}) => {
    return (
        <Button className={styles.button}
            type="submit"
            disabled={disabled}
            onClick={onClick}>
                {title}
        </Button>
    )
}

export default ButtonLanjut
