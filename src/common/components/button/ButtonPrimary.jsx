import { Button } from 'react-bootstrap';

import styles from './ButtonPrimary.module.scss';

const ButtonPrimary = ({
  children,
  disabled,
  onClick,
  className: userClassName,
  styles: userStyles,
  type: userType,
}) => {
  const buttonClassName = userClassName ? userClassName : styles.btnPrimary;

  return (
    <Button
      className={buttonClassName}
      type={userType ? userType : 'submit'}
      disabled={disabled}
      onClick={onClick ? onClick : () => {}}
      styles={userStyles ? userStyles : ''}>
      {children}
    </Button>
  );
};

export default ButtonPrimary;
