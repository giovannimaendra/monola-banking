import { Button } from 'react-bootstrap';

import styles from './ButtonNext.module.scss';

const ButtonNext = ({ children, disabled, onClick, type }) => {
  return (
    <Button
      disabled={disabled}
      onClick={onClick}
      className={styles.btnNext}
      type={type ? type : 'submit'}>
      {children}
    </Button>
  );
};

export default ButtonNext;
