import { Button } from 'react-bootstrap';

import styles from './ButtonSubmitSaving.module.scss';

const ButtonSubmitSaving = ({ children, disabled, onClick }) => {
  return (
    <Button
      className={styles.button}
      type="submit"
      disabled={disabled}
      onClick={onClick}>
      {children}
    </Button>
  );
};

export default ButtonSubmitSaving;
