import { Button } from 'react-bootstrap';

import styles from './ButtonPrevious.module.scss';

const ButtonPrevious = ({ children, onClick }) => {
  return (
    <Button onClick={onClick} className={styles.btnPrevious}>
      {children}
    </Button>
  );
};

export default ButtonPrevious;
