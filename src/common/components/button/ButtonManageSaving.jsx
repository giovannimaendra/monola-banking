import styles from './ButtonManageSaving.module.scss';
import { Button } from 'react-bootstrap';
import { classNameJoin } from '../../../services/helpers';

const ButtonManageSaving = ({children, onClick, className}) => {
    const buttonCN = classNameJoin([styles.button, className]);
    return (
        <Button className={buttonCN} onClick={onClick}>{children}</Button>
    )
}

export default ButtonManageSaving
