import { forwardRef } from 'react';
import { BiChevronDown } from 'react-icons/bi';

import styles from './CustomDateInput.module.scss';

const CustomDateInput = forwardRef(({ value, onClick }, ref) => {
  return (
    <button
      className={styles.customDateInput}
      ref={ref}
      onClick={onClick}
      type="button">
      <input
        type="text"
        disabled
        placeholder={!!value ? '' : 'Pilih Tanggal'}
        className={!!value ? styles.innerInputHidden : styles.innerInput}
      />
      {value}
      <BiChevronDown className={styles.customChevron} />
    </button>
  );
});

export default CustomDateInput;
