import { classNameJoin } from '../../../services/helpers';
import styles from './IconItem.module.scss';

const IconItem = ({ component, onClick, iconName, selectedIcon }) => {

  const containerComponentCN = classNameJoin([styles.containerComponent, iconName === selectedIcon ? styles.componentPicked : '']);

  return (
    <div className={containerComponentCN} onClick={onClick}>
      {component}
    </div>
  )
}

export default IconItem
