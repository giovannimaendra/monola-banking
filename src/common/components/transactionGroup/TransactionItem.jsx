import React from 'react';
import { ListGroup } from 'react-bootstrap';
import { useSelector } from 'react-redux';

import { getVendorComponent } from '../../../services/helpers';

import { classNameJoin } from '../../../services/helpers';

import styles from './TransactionItem.module.scss';

const TransactionItem = React.memo(({ itemId = null, onClick, selector }) => {
  const { userName, provider, identityNumber, receiverId } = useSelector(
    !!itemId ? (state) => selector(state, itemId) : selector
  );

  const Vendor = getVendorComponent(provider);

  const transactionItemClassName = classNameJoin([
    'my-2',
    styles.transactionItem,
    onClick ? styles.linkItem : '',
  ]);

  return (
    <ListGroup.Item
      className={transactionItemClassName}
      onClick={onClick ? () => onClick(receiverId) : () => {}}>
      <div className="row no-gutters">
        <div className="col-1 ">
          <Vendor />
        </div>
        <div className="col d-flex align-items-center">
          <p className={styles.name}>{userName}</p>
        </div>
        <div className="col-3 d-flex align-items-center justify-content-end">
          <p className={styles.identityNumber}>{identityNumber}</p>
        </div>
      </div>
    </ListGroup.Item>
  );
});

export default TransactionItem;
