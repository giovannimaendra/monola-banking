import DatePicker, { CalendarContainer } from 'react-datepicker';
import { getYear, getMonth, format, getDay } from 'date-fns';
import { BiChevronRight, BiChevronLeft } from 'react-icons/bi';

import CustomDateInput from '../customdateinput/CustomDateInput';

import styles from './CustomCalendar.module.scss';

import 'react-datepicker/dist/react-datepicker.css';

const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const shortMonths = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];

const CustomCalendar = ({ selected, onChange }) => {
  return (
    <DatePicker
      minDate={new Date()}
      dateFormat="dd MMMM yyyy"
      withPortal
      calendarContainer={({ children, className }) => (
        <div style={{ backgroundColor: 'white' }}>
          <CalendarContainer className={className}>
            <div className="position-relative">{children}</div>
          </CalendarContainer>
        </div>
      )}
      renderCustomHeader={({ date, decreaseMonth, increaseMonth }) => (
        <>
          <div className={styles.calendarHeader}>
            <p className={styles.calendarHeaderYear}>{getYear(new Date())}</p>
            <p className={styles.calendarHeaderDate}>
              {days[getDay(date)]}, {shortMonths[getMonth(date)]}{' '}
              {format(date, 'yy')}
            </p>
          </div>
          <div className={styles.calendarMonthChanger}>
            <BiChevronLeft
              onClick={decreaseMonth}
              className={styles.monthChangerIcon}
            />
            <span className={styles.monthChangerText}>
              {months[getMonth(date)]} {getYear(date)}
            </span>
            <BiChevronRight
              onClick={increaseMonth}
              className={styles.monthChangerIcon}
            />
          </div>
        </>
      )}
      customInput={<CustomDateInput />}
      selected={selected}
      onChange={onChange}
    />
  );
};

export default CustomCalendar;
