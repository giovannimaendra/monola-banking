import { useSelector } from 'react-redux';

import styles from './Coin.module.scss';

import { classNameJoin } from '../../../services/helpers';

import CoinImg from '../../../assets/img/coin.svg';
import { userSelectors } from '../../../features/user';
import CoinLoader from '../Loader/CoinLoader';

const Coin = ({ coin, coinShop, className, disabled }) => {
  const coinCN = classNameJoin([
    styles.coinContainer,
    coinShop ? styles.isCoinShop : className,
    disabled && styles.disabled,
  ]);

  const coinFetchingStatus = useSelector(
    userSelectors.selectUserCoinsFetchingStatus
  );

  return (
    <div className={coinCN}>
      <img className={styles.coin} src={CoinImg} alt="" />
      <span className={styles.coinText}>
        {coin && !disabled ? (
          coin
        ) : coinFetchingStatus === 'pending' ? (
          <CoinLoader />
        ) : (
          'Owned'
        )}
      </span>
    </div>
  );
};

export default Coin;
