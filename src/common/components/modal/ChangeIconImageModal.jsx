import { Modal, Button } from 'react-bootstrap';
import { AiOutlineCloseCircle } from 'react-icons/ai';

import styles from './ChangeIconImageModal.module.scss';

import IconItem from '../iconItem/IconItem';

import { classNameJoin, renderSavingIcon } from '../../../services/helpers';

const ChangeIconImageModal = (props) => {
  const { selectedIcon, onClickIconItem } = props;

  const handleClickSimpan = () => {
    props.onClickSimpan(selectedIcon);
  };

  const renderedSavingIcon = renderSavingIcon().map(
    ({ Component, id, goalIcon }) => (
      <IconItem
        key={id}
        iconName={goalIcon}
        selectedIcon={selectedIcon}
        onClick={() => onClickIconItem(goalIcon)}
        component={<Component className={styles.component} />}
      />
    )
  );

  const containerRenderedIconCN = classNameJoin([
    'd-flex flex-wrap justify-content-between',
    styles.containerRenderedIcon,
  ]);

  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      centered
      aria-labelledby="container-modal-title-vcenter"
      size="lg"
      className={styles.modal}>
      <Modal.Header className={styles.modalHeader}>
        <div className="d-flex justify-content-between w-100">
          <span className={styles.title}>Ganti Gambar</span>
          <AiOutlineCloseCircle
            className={styles.close}
            size="22px"
            color="#9E9E9E"
            onClick={props.onHide}
          />
        </div>
      </Modal.Header>
      <div className={styles.divider} />
      <Modal.Body className={styles.modalBody}>
        <div className={containerRenderedIconCN}>{renderedSavingIcon}</div>
      </Modal.Body>
      <Modal.Footer className={styles.modalFooter}>
        <Button className={styles.btnOnHide} onClick={props.onHide}>
          Batal
        </Button>
        <Button className={styles.btnAccept} onClick={handleClickSimpan}>
          Simpan
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ChangeIconImageModal;
