import { useState } from 'react';
import { Modal, Form, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { userThunks } from '../../../features/user';

import styles from './ChangeNameModal.module.scss';

const ChangeNameModal = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [length, setLength] = useState(0);
  const [inputNama, setInputNama] = useState('');

  const handleInputNama = (e) => {
    setInputNama(e.target.value);
    setLength(e.target.value.length);
  };

  const handlePetNameChange = () => {
    dispatch(userThunks.changeUserPetName(inputNama));

    props.onHide();
    history.replace('/dashboard/saving');
  };

  return (
    <Modal
      {...props}
      centered
      aria-labelledby="contained-modal-title-vcenter"
      size="lg"
      className={styles.modal}>
      <Modal.Body className={styles.modalBody}>
        <Form className={styles.form}>
          <Form.Group className={styles.formGroup} controlId="name">
            <Form.Label className={styles.label}>Ganti Nama</Form.Label>
            <Form.Control
              // maxLength="10"
              className={styles.input}
              type="input"
              placeholder="Masukan Nama Petmu"
              value={inputNama}
              onChange={handleInputNama}
            />
          </Form.Group>
          <div className="d-flex align-self-end">
            <span className={styles.limit}>{length}</span>
            <span className={styles.limit}>/10</span>
          </div>

          <div className={styles.buttonGroup}>
            <Button
              className={styles.batalBtn}
              variant="link"
              onClick={props.onHide}>
              BATAL
            </Button>
            <Button
              className={styles.simpanBtn}
              variant="link"
              type="button"
              onClick={handlePetNameChange}>
              SIMPAN
            </Button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

export default ChangeNameModal;
