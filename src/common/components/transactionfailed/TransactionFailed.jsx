import { useEffect } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button } from 'react-bootstrap';

import { transferActions } from '../../../features/transfer';
import { eWalletActions } from '../../../features/ewallet';

import { ReactComponent as Failed } from '../../../assets/img/Failed.svg';
import { classNameJoin } from '../../../services/helpers';

import styles from './TransactionFailed.module.scss';

const TransactionFailed = () => {
  const location = useLocation();
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    return history.listen((location) => {
      if (history.action === 'POP') {
        if (location.pathname.includes('ewallet')) {
          dispatch(eWalletActions.resetTopUpStatus());
          history.replace(
            '/dashboard/ewallet/top-up-ewallet/fill-information#step2'
          );
          return;
        }
        dispatch(transferActions.resetTransferStatus());
        history.replace('/dashboard/transaksi/transfer/fill-information#step2');
      }
    });
  }, [history, dispatch]);

  const handleBackClick = () => {
    if (location.pathname.includes('ewallet')) {
      dispatch(eWalletActions.resetTopUpStatus());
      history.replace(
        '/dashboard/ewallet/top-up-ewallet/fill-information#step2'
      );
      return;
    }
    dispatch(transferActions.resetTransferStatus());
    history.replace('/dashboard/transaksi/transfer/fill-information#step2');
  };

  const successContainerClassName = classNameJoin([
    'd-flex flex-column',
    styles.container,
  ]);

  const headingContainerClassName = classNameJoin([
    'd-flex flex-column align-items-center justify-content-between',
    styles.headingContainer,
  ]);

  return (
    <div className={successContainerClassName}>
      <div className={headingContainerClassName}>
        <Failed className="u-margin-top-large" />
        <h3 className={styles.successHeading}>
          {location.pathname.includes('ewallet') ? 'Top Up' : 'Transfer'} Gagal!
        </h3>
      </div>
      <div className={styles.detailContainer}>
        <p className={styles.detailText}>
          Mohon maaf transaksi yang anda lakukan tidak dapat diproses. Coba
          ulangi beberapa saat lagi!
        </p>
        <Button className={styles.backButton} onClick={handleBackClick}>
          Kembali
        </Button>
      </div>
    </div>
  );
};

export default TransactionFailed;
