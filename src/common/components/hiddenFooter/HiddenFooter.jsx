import { Button } from 'react-bootstrap';

import { classNameJoin } from '../../../services/helpers';

import Monola from '../monola/Monola';
import styles from './HiddenFooter.module.scss';
import { MdClose } from 'react-icons/md';

const HiddenFooter = ({ onClick, isHidden }) => {
  if (isHidden) {
    return null;
  }

  const titleContainerClassName = classNameJoin([
    'col-6 text-center d-sm-flex justify-content-sm-center',
    styles.titleContainer,
  ]);

  const monolaContainerClassName = classNameJoin([
    'col-3 text-center',
    styles.monolaContainer,
  ]);

  const mainContainerClassName = classNameJoin([
    'row align-items-center justify-content-center h-100 mx-sm-3',
    styles.mainContainer,
  ]);

  return (
    <div className={styles.hiddenFooter}>
      <div className="h-100 position-relative">
        <div className={styles.icon}>
          <MdClose
            className={styles.iconX}
            color="black"
            size="20"
            onClick={onClick}
          />
        </div>
        <div className="mx-5 h-100">
          <div className={mainContainerClassName}>
            <div className={monolaContainerClassName}>
              <Monola size="lg-2" />
            </div>
            <div className={titleContainerClassName}>
              <h4 className={styles.title}>Make Banking Easy and Fun!</h4>
            </div>
            <div className="col-3 text-center">
              <Button
                className={styles.btnWhite}
                href="http://play.google.com"
                target="_blank">
                BUKA
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HiddenFooter;
