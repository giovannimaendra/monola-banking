import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { getVendorComponent } from '../../../services/helpers';

import { eWalletSelectors, eWalletActions } from '../../../features/ewallet';

import styles from './EWalletItem.module.scss';

const EWalletItem = ({ eWalletId }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const { userName, provider, identityNumber } = useSelector((state) =>
    eWalletSelectors.selectFavoriteEWalletById(state, eWalletId)
  );

  const Vendor = getVendorComponent(provider);

  const handleItemClick = () => {
    dispatch(
      eWalletActions.setEditingEWallet({
        userName,
        provider,
        identityNumber,
      })
    );

    history.push(`/dashboard/ewallet/detail-ewallet?ewalletName=${userName}`);
  };

  return (
    <div className={styles.eWalletItem} onClick={handleItemClick}>
      <div className={styles.content}>
        <Vendor />
        <p className={styles.userName}>{userName}</p>
      </div>
    </div>
  );
};

export default EWalletItem;
