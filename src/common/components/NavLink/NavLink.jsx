import { classNameJoin } from '../../../services/helpers';

import styles from './NavLink.module.scss';

const NavLink = ({
  children,
  activeClassName,
  className: userClassName,
  isActive,
  onClick,
}) => {
  const navLinkClassName = classNameJoin([
    styles.navLink,
    userClassName && userClassName,
    isActive && activeClassName,
  ]);

  return (
    <div onClick={onClick ? onClick : () => {}} className={navLinkClassName}>
      {children}
    </div>
  );
};

export default NavLink;
