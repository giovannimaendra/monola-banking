import { classNameJoin } from '../../../services/helpers';

import styles from './UserCard.module.scss';

const UserCard = ({ children, small, variant }) => {
  const userCardClassName = classNameJoin([
    small ? styles.userCardSm : '',
    variant ? styles.userCardVar : '',
    styles.userCard,
  ]);

  return <div className={userCardClassName}>{children}</div>;
};

export default UserCard;
