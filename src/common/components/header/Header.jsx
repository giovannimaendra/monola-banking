import { useLocation, Link } from 'react-router-dom';
import { IoMdNotificationsOutline } from 'react-icons/io';
import { useSelector } from 'react-redux';

import { classNameJoin } from '../../../services/helpers';

import Monola from '../monola/Monola';
import styles from './Header.module.scss';
import { userSelectors } from '../../../features/user';

const Header = () => {
  const isLoggedIn = useSelector(userSelectors.selectIsLoggedIn);
  const location = useLocation();

  const headerClassName = classNameJoin([
    'navbar navbar-expand-lg navbar-light bg-light',
    styles.header,
    !location.pathname.includes('dashboard') ? 'fixed-top' : '',
  ]);

  const { containerInside, login } = styles;

  const { container, loginWrapper } = {
    container: classNameJoin([
      'container p-2 align-items-center mx-auto w-75',
      containerInside,
    ]),
    loginWrapper: classNameJoin(['navbar-nav ml-auto', login]),
  };

  const shouldRenderLoginBtn = isLoggedIn ? (
    <IoMdNotificationsOutline className={styles.icon} />
  ) : (
    <div className={loginWrapper}>
      <Link to="/login" className={styles.btnLogin}>
        Login
      </Link>
    </div>
  );

  return (
    <nav className={headerClassName}>
      <div className={container}>
        <Monola />
        {shouldRenderLoginBtn}
      </div>
    </nav>
  );
};

export default Header;
