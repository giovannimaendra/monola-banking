import styles from './TitleHeader.module.scss';

import { classNameJoin } from '../../../services/helpers';

const TitleHeader = ({ className, userName }) => {
    const titleHeaderClassName = classNameJoin([styles.titleHeader, className])

    return (
        <div className={titleHeaderClassName}>
            <h4 className={styles.nameUser}>Hello, {userName}!</h4>
        </div>
    )
}

export default TitleHeader
