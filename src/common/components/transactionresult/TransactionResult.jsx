import { Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';

import {
  checkObjectIsNotEmpty,
  separatorWithDot,
} from '../../../services/helpers';

import ButtonNext from '../button/ButtonNext';
import ButtonPrevious from '../button/ButtonPrevious';
import TransactionItem from '../transactionGroup/TransactionItem';

import { transferActions, transferSelectors } from '../../../features/transfer';

import styles from './TransactionResult.module.scss';
import { eWalletSelectors } from '../../../features/ewallet';

const TransactionResult = ({
  recipient,
  nominal,
  description,
  schedule,
  onCheck,
  isFavorited,
  nextStep,
  previousStep,
}) => {
  let scheduleText = '';

  switch (schedule) {
    case 'SEKARANG':
      scheduleText = 'Sekarang';
      break;
    case 'TANGGAL_TERTENTU':
      scheduleText = 'Pada Tanggal Tertentu';
      break;
    case 'SETIAP_N_HARI_BULAN':
      scheduleText = 'Setiap N Hari/Bulan';
      break;
    case 'SETIAP_MINGGU':
      scheduleText = 'Setiap Minggu';
      break;
    case 'SETIAP_BULAN':
      scheduleText = 'Setiap Bulan';
      break;
    default:
      break;
  }

  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();

  const fetchRecipientStatus = useSelector(
    transferSelectors.selectFetchRecipientStatus
  );

  const fetchEWalletStatus = useSelector(
    eWalletSelectors.selectEWalletRecipientFetchStatus
  );

  const doesExists = useSelector((state) => state.transfer.recipientDoesExists);

  const shouldRenderSchedule = location.pathname.includes('ewallet') ? null : (
    <div className="row no-gutters py-2">
      <div className="col">Kapan</div>
      <div className="col text-right">{scheduleText}</div>
    </div>
  );

  const shouldRenderFavoriteCheckBox = location.pathname.includes(
    'ewallet'
  ) ? null : (
    <>
      <Form.Group>
        <Form.Check
          type="checkbox"
          label="Masukkan akun bank ke daftar favorit"
          onChange={onCheck}
          checked={isFavorited}
          disabled={doesExists}
        />
        {!!doesExists && <Form.Text>Akun sudah ada di favorit.</Form.Text>}
      </Form.Group>
    </>
  );

  const handlePreviousClick = () => {
    dispatch(transferActions.setIsFavorite(isFavorited));
    history.push('/dashboard/cancel-transaction');
  };

  if (
    location.pathname.includes('ewallet')
      ? !checkObjectIsNotEmpty(recipient) && fetchEWalletStatus === 'idle'
      : !recipient && fetchRecipientStatus === 'idle'
  ) {
    previousStep();
  }

  const whichSelector = location.pathname.includes('transfer')
    ? transferSelectors.selectRecipient
    : eWalletSelectors.selectEWalletRecipient;

  return (
    <>
      {(location.pathname.includes('ewallet')
        ? checkObjectIsNotEmpty(recipient)
        : recipient) && <TransactionItem selector={whichSelector} />}
      <div className={styles.overviewContainer}>
        <div className="row no-gutters py-2 mb-2">
          <div className="col">Nominal Transfer</div>
          <div className="col text-right">
            <span className={styles.nominal}>
              Rp {separatorWithDot(nominal)}
            </span>
          </div>
        </div>
        <div className="row no-gutters py-2 my-3">
          <div className="col">Deskripsi</div>
          <div className="col text-right">
            {description ? description : '-'}
          </div>
        </div>
        {shouldRenderSchedule}
      </div>
      {shouldRenderFavoriteCheckBox}
      <Form.Group className="py-4">
        <div className="row justify-content-between">
          <div className="col-6">
            <ButtonPrevious onClick={handlePreviousClick}>
              Kembali
            </ButtonPrevious>
          </div>
          <div className="col-6">
            <ButtonNext onClick={nextStep}>
              {location.pathname.includes('ewallet') ? 'Top Up' : 'Transfer'}
            </ButtonNext>
          </div>
        </div>
      </Form.Group>
    </>
  );
};

export default TransactionResult;
