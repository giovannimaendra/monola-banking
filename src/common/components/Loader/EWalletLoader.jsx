import React from 'react';
import ContentLoader from 'react-content-loader';

const EWalletLoader = (props) => (
  <ContentLoader
    speed={2}
    width={600}
    height={150}
    viewBox="0 0 600 150"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}>
    <rect x="0" y="24" rx="3" ry="3" width="128" height="106" />
    <rect x="583" y="27" rx="3" ry="3" width="128" height="106" />
    <rect x="146" y="25" rx="3" ry="3" width="128" height="106" />
    <rect x="295" y="24" rx="3" ry="3" width="128" height="106" />
    <rect x="440" y="26" rx="3" ry="3" width="128" height="106" />
  </ContentLoader>
);

export default EWalletLoader;
