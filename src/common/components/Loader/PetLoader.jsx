import React from 'react';
import ContentLoader from 'react-content-loader';

const PetLoader = (props) => (
  <ContentLoader
    speed={2}
    width={500}
    height={300}
    viewBox="0 0 500 300"
    backgroundColor="#a1a1a1"
    foregroundColor="#ecebeb"
    {...props}>
    <rect x="196" y="3" rx="5" ry="5" width="115" height="163" />
    <rect x="196" y="203" rx="5" ry="5" width="115" height="26" />
    <rect x="170" y="250" rx="5" ry="5" width="37" height="35" />
    <rect x="235" y="249" rx="5" ry="5" width="37" height="35" />
    <rect x="300" y="249" rx="5" ry="5" width="37" height="35" />
  </ContentLoader>
);

export default PetLoader;
