import React from 'react';
import ContentLoader from 'react-content-loader';

const CoinLoader = (props) => (
  <ContentLoader
    speed={2}
    width={50}
    height={20}
    viewBox="0 0 100 50"
    backgroundColor="#a1a1a1"
    foregroundColor="#ecebeb"
    {...props}>
    <rect x="60" y="20" rx="5" ry="5" width="50" height="10" />
    <circle cx="20" cy="25" r="20" />
  </ContentLoader>
);

export default CoinLoader;
