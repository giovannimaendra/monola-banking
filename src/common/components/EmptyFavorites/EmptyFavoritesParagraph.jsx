const EmptyFavoritesParagraph = ({ children }) => {
  return <p className="empty-favorite__paragraph">{children}</p>;
};

export default EmptyFavoritesParagraph;
