import styles from './EmptyFavorites.module.scss';

const EmptyFavorites = ({ children }) => {
  return (
    <div className={styles.emptyImg}>
      <div className="w-50 empty-favorite">{children}</div>
    </div>
  );
};

export default EmptyFavorites;
