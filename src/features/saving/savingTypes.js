const fetchSaving = {
  pending: 'saving/fetchSavingPending',
  fulfilled: 'saving/fetchSavingFulfilled',
  rejected: 'saving/fetchSavingRejected',
};

const getAndSetManagedSaving = {
  pending: 'saving/getAndSetManagedSavingPending',
  fulfilled: 'saving/getAndSetManagedSavingFulfilled',
  rejected: 'saving/getAndSetManagedSavingRejected',
};

const setSavingItem = 'saving/setSavingItem';

const resetSaving = 'saving/resetSaving';

const updateSavings = {
  pending: 'saving/updateSavingsPending',
  fulfilled: 'saving/updateSavingsFulfilled',
  rejected: 'saving/updateSavingsRejected',
};

export {
  fetchSaving,
  setSavingItem,
  resetSaving,
  updateSavings,
  getAndSetManagedSaving,
};
