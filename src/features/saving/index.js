import reducer from './savingReducer';

import * as savingThunks from './savingThunks';
import * as savingTypes from './savingTypes';
import * as savingActions from './savingActions';
import * as savingSelectors from './savingSelectors';

export { savingThunks, savingTypes, savingActions, savingSelectors };

export default reducer;
