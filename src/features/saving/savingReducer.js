import * as types from './savingTypes';
import { userTypes } from '../user';

import { IDLE, PENDING, FULFILLED, REJECTED } from '../../services/enums';

const INITIAL_STATE = {
  savings: [],
  managedSaving: {},
  userSavingsFetchingStatus: IDLE,
  managedSavingFetchingStatus: IDLE,
  addNewSavingGoalStatus: IDLE,
  addNewSavingGoalError: false,
  managedSavingFetchingError: false,
  userSavingsFetchingError: false,
};

const savingReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.fetchSaving.pending: {
      return {
        ...state,
        savings: [],
        userSavingsFetchingStatus: PENDING,
        userSavingsFetchingError: false,
      };
    }

    case types.fetchSaving.fulfilled: {
      const userSavings = action.payload;
      return {
        ...state,
        savings: userSavings,
        userSavingsFetchingStatus: FULFILLED,
      };
    }

    case types.fetchSaving.rejected: {
      return {
        ...state,
        userSavingsFetchingStatus: REJECTED,
        userSavingsFetchingError: true,
      };
    }

    case types.getAndSetManagedSaving.pending: {
      return {
        ...state,
        savings: [],
        managedSavingFetchingStatus: PENDING,
        managedSavingFetchingError: false,
      };
    }

    case types.getAndSetManagedSaving.fulfilled: {
      const { savings, savingId } = action.payload;

      const managedSaving = savings.find((saving) => saving.id === savingId);

      return {
        ...state,
        savings,
        managedSaving,
        managedSavingFetchingStatus: FULFILLED,
      };
    }

    case types.getAndSetManagedSaving.rejected: {
      return {
        ...state,
        managedSavingFetchingStatus: REJECTED,
        managedSavingFetchingError: true,
      };
    }

    case types.setSavingItem: {
      const savingItem = action.payload;
      return { ...state, savingItem };
    }

    case types.resetSaving: {
      return (state = INITIAL_STATE);
    }

    case types.updateSavings.fulfilled: {
      const infoObj = action.payload;
      return {
        ...state,
        savings: [...state.savings, infoObj],
        addNewSavingGoalStatus: FULFILLED,
      };
    }

    case userTypes.logout: {
      return INITIAL_STATE;
    }

    default:
      return state;
  }
};

export default savingReducer;
