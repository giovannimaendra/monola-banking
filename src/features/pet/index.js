import reducer from './petReducer';

import * as petActions from './petActions';
import * as petThunks from './petThunks';
import * as petTypes from './petTypes';
import * as petSelectors from './petSelectors';

export { petActions, petTypes, petSelectors, petThunks };

export default reducer;
