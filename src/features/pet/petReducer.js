import * as types from './petTypes';
import { userTypes } from '../user';

import { IDLE, PENDING, FULFILLED, REJECTED } from '../../services/enums';

import { formatItemObjectData } from '../../services/helpers';

const INITIAL_STATE = {
  pets: [],
  petFetchingStatus: IDLE,
  petFetchingError: false,
};

const petReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.fetchPets.pending: {
      return {
        ...state,
        pets: [],
        petFetchingStatus: PENDING,
        petFetchingError: false,
      };
    }

    case types.fetchPets.fulfilled: {
      const pets = action.payload.map((pet) =>
        formatItemObjectData(pet.petId, pet.petName, pet.petImage, pet.petPrice)
      );

      return { ...state, petFetchingStatus: FULFILLED, pets };
    }

    case types.fetchPets.rejected: {
      return { ...state, petFetchingStatus: REJECTED, petFetchingError: true };
    }

    case userTypes.logout: {
      return INITIAL_STATE;
    }

    default:
      return state;
  }
};

export default petReducer;
