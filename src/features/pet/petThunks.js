import * as actions from './petActions';
import * as apis from './petAPI';

import { userAPI, userThunks } from '../user';

const fetchPets = () => async (dispatch) => {
  dispatch(actions.fetchPets.pending());

  try {
    const { content } = await apis.fetchPets();

    dispatch(actions.fetchPets.fulfilled(content));
  } catch (err) {
    dispatch(actions.fetchPets.rejected());
  }
};

const purchasePet = (petId) => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  dispatch(actions.purchasePet.pending());

  const petObj = {
    isChosen: true,
    petId,
    userId,
  };

  const updateObj = {
    petId,
  };

  try {
    await apis.postUserPet(petObj);

    await userAPI.updateUserCurrentVariant(userId, updateObj);

    dispatch(userThunks.fetchUserCurrentVariant());
    dispatch(userThunks.fetchUserCoins());
    dispatch(actions.purchasePet.fulfilled());
  } catch (err) {
    dispatch(actions.purchasePet.rejected());
  }
};

export { fetchPets, purchasePet };
