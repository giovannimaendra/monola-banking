import * as types from './balanceTypes';

const fetchUserBalance = {
  pending() {
    return { type: types.fetchUserBalance.pending };
  },
  fulfilled(balance) {
    return { type: types.fetchUserBalance.fulfilled, payload: balance };
  },
  rejected() {
    return { type: types.fetchUserBalance.rejected };
  },
};

const deductUserBalance = (deductionAmount) => ({
  type: types.deductUserBalance,
  payload: deductionAmount,
});

const addUserBalance = (addAmount) => ({
  type: types.addUserBalance,
  payload: addAmount,
});

export { fetchUserBalance, deductUserBalance, addUserBalance };
