import { getUserBalance } from './balanceAPI';

import * as actions from './balanceActions';

const fetchUserBalance = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;
  dispatch(actions.fetchUserBalance.pending());

  try {
    const { content } = await getUserBalance(userId);

    const balance = content.balance;

    dispatch(actions.fetchUserBalance.fulfilled(balance));
  } catch (err) {
    dispatch(actions.fetchUserBalance.rejected());
  }
};

export { fetchUserBalance };
