import monola from '../../services/apis/monola';

export const getUserBalance = async (userId) => {
  const response = await monola.get(`/core/balances/${userId}`);

  return response.data;
};
