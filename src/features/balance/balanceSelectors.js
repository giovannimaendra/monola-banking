const selectUserBalance = (state) => state.balance.userBalance;

const selectBalanceFetchStatus = (state) => state.balance.status;

export { selectUserBalance, selectBalanceFetchStatus };
