import reducer from './historyReducer';

import * as historyActions from './historyActions';
import * as historySelectors from './historySelectors';
import * as historyThunks from './historyThunks';
import * as historyTypes from './historyTypes';

export { historyActions, historySelectors, historyThunks, historyTypes };

export default reducer;