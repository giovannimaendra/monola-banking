import * as types from './historyTypes';

const fetchHistoryTransaction = {
  pending() {
    return { type: types.fetchHistoryTransaction.pending };
  },
  fulfilled(historyTransaction) {
    return { 
      type: types.fetchHistoryTransaction.fulfilled,
      payload: historyTransaction };
  },
  rejected() {
    return { type: types.fetchHistoryTransaction.rejected };
  },
};

export { fetchHistoryTransaction };