import monola from '../../services/apis/monola';

const fetchHistoryTransaction = async (userId) => {
  const response = await monola.get('/core/transfers/histories', { params: { userId }});

  return response.data;
};

export { fetchHistoryTransaction };