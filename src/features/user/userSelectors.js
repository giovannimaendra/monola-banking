import { createSelector } from 'reselect';

const selectUser = (state) => state.user.user;

const selectUserProfile = (state) => state.user.userProfile;

const selectUserId = (state) => state.user.user.userId;

const selectUserCoins = (state) => state.user.user.userCoin;

const selectUserCurrentPet = (state) => state.user.currentUserPet;

const selectUserDisplayedPet = (state) => state.user.currentDisplayedPetVariant;

const selectUserPets = (state) => state.user.userPets;

const selectUserPetById = createSelector(
  [selectUserPets, (_, petId) => petId],
  (pets, petId) => pets.find((pet) => pet.id === petId)
);

const selectUserAccessories = (state) => state.user.userAccessories;

const selectUserAccessoryById = createSelector(
  [selectUserAccessories, (_, accId) => accId],
  (accessories, accId) =>
    accessories.find((accessory) => accessory.id === accId)
);

const selectUserInventory = (state) => state.user.userInventory;

const selectUserItemInInventory = createSelector(
  [selectUserInventory, (_, itemId) => itemId],
  (inventory, itemId) => inventory.find((item) => item.id === itemId)
);

const selectIsLoggedIn = (state) => state.user.isLoggedIn;

const selectUserFetchingStatus = (state) => state.user.fetchUserStatus;

const selectUserProfileFetchingStatus = (state) =>
  state.user.fetchUserProfileStatus;

const selectUserCurrentPetFetchingStatus = (state) =>
  state.user.fetchUserCurrentPetStatus;

const selectUserPetsFetchingStatus = (state) => state.user.fetchUserPetsStatus;

const selectUserAccessoriesFetchingStatus = (state) =>
  state.user.fetchUserAccessoriesStatus;

const selectUserCurrentVariantFetchingStatus = (state) =>
  state.user.fetchUserVariantStatus;

const selectLoginStatus = (state) => state.user.loginStatus;

const selectUserCoinsFetchingStatus = (state) =>
  state.user.fetchUserCoinsStatus;

const selectLoginError = (state) => state.user.loginError;

export {
  selectUser,
  selectUserProfile,
  selectUserId,
  selectIsLoggedIn,
  selectUserFetchingStatus,
  selectUserProfileFetchingStatus,
  selectUserDisplayedPet,
  selectUserInventory,
  selectUserItemInInventory,
  selectUserCoins,
  selectUserCurrentPet,
  selectUserPets,
  selectUserPetById,
  selectUserAccessories,
  selectUserAccessoryById,
  selectUserPetsFetchingStatus,
  selectUserCurrentPetFetchingStatus,
  selectUserAccessoriesFetchingStatus,
  selectUserCurrentVariantFetchingStatus,
  selectUserCoinsFetchingStatus,
  selectLoginStatus,
  selectLoginError,
};
