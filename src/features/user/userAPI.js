import monola from '../../services/apis/monola';

export const login = async (userData) => {
  // const response = await monola.get(`/core/balances/${userId}`);

  const response = await monola.post('/auth/login', userData);

  return response.data;
};

export const fetchUserData = async (userId) => {
  const response = await monola.get(`/core/balances/${userId}`);

  return response.data;
};

export const fetchUserProfile = async (userId) => {
  const response = await monola.get(`/user-profile/${userId}`);

  return response.data;
};

export const fetchUserCoins = async (userId) => {
  const response = await monola.get(`/user-profile/coin/${userId}`);

  return response.data;
};

export const fetchUserPets = async (userId) => {
  const response = await monola.get('user-pets/my-pets', {
    params: { userId },
  });

  return response.data;
};

export const fetchUserAccessories = async (userId) => {
  const response = await monola.get(
    `user-accessories/my-accessories?userId=${userId}`
  );

  return response.data;
};

export const fetchUserCurrentPet = async (userId) => {
  const response = await monola.get(`user-pets/current?userId=${userId}`);
  // const response = await monola.get(
  //   `user-variants/my-variant?userId=${userId}`
  // );

  return response.data;
};

export const fetchUserCurrentVariant = async (userId) => {
  const response = await monola.get(
    `user-variants/my-variant?userId=${userId}`
  );

  return response.data;
};

export const updateUserCurrentVariant = async (userId, updateObj) => {
  const response = await monola.put(
    `user-variants/my-variant?userId=${userId}`,
    updateObj
  );

  return response.data;
};

export const purchaseAccessories = async (purchaseObj) => {
  const response = await monola.post('user-accessories', purchaseObj);

  return response.data;
};

export const updateUserPet = async (petId, userId) => {
  const response = await monola.put(
    `user-pets?petId=${petId}&userId=${userId}`
  );

  return response.data;
};

export const switchAccessory = async (accId, userId) => {
  const response = await monola.put(
    `user-accessories?accessoryId=${accId}&userId=${userId}`
  );

  return response.data;
};

export const changeUserPetName = async (newPetName, userId) => {
  const response = await monola.put(
    `user-variants/name?userId=${userId}`,
    newPetName
  );

  return response.data;
};
