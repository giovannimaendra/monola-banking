import reducer from './userReducer';

import * as userActions from './userActions';
import * as userTypes from './userTypes';
import * as userThunks from './userThunks';
import * as userSelectors from './userSelectors';
import * as userAPI from './userAPI';

export { userActions, userTypes, userThunks, userSelectors, userAPI };

export default reducer;
