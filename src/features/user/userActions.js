import * as types from './userTypes';

const login = {
  pending() {
    return { type: types.login.pending };
  },
  fulfilled(userId) {
    return { type: types.login.fulfilled, payload: userId };
  },
  rejected() {
    return { type: types.login.rejected };
  },
};

const setUserId = (userId) => ({ type: types.setUserId, payload: userId });

const setUserBalance = (balance) => ({
  type: types.setUserBalance,
  payload: balance,
});

const fetchUserData = {
  pending() {
    return { type: types.fetchUserData.pending };
  },
  fulfilled(userData) {
    return { type: types.fetchUserData.fulfilled, payload: userData };
  },
  rejected() {
    return { type: types.fetchUserData.rejected };
  },
};

const fetchUserProfile = {
  pending() {
    return { type: types.fetchUserProfile.pending };
  },
  fulfilled(userProfile) {
    return { type: types.fetchUserProfile.fulfilled, payload: userProfile };
  },
  rejected() {
    return { type: types.fetchUserProfile.rejected };
  },
};

const logout = () => ({ type: types.logout });

const fetchUserCoins = {
  pending() {
    return { type: types.fetchUserCoins.pending };
  },
  fulfilled(userCoins) {
    return { type: types.fetchUserCoins.fulfilled, payload: userCoins };
  },
  rejected() {
    return { type: types.fetchUserCoins.rejected };
  },
};

const fetchUserPets = {
  pending() {
    return { type: types.fetchUserPets.pending };
  },
  fulfilled(userPets) {
    return { type: types.fetchUserPets.fulfilled, payload: userPets };
  },
  rejected() {
    return { type: types.fetchUserPets.rejected };
  },
};

const fetchUserCurrentPet = {
  pending() {
    return { type: types.fetchUserCurrentPet.pending };
  },
  fulfilled(userCurrentPet) {
    return {
      type: types.fetchUserCurrentPet.fulfilled,
      payload: userCurrentPet,
    };
  },
  rejected() {
    return { type: types.fetchUserCurrentPet.rejected };
  },
};

const fetchUserCurrentVariant = {
  pending() {
    return { type: types.fetchUserCurrentVariant.pending };
  },
  fulfilled(userVariant) {
    return {
      type: types.fetchUserCurrentVariant.fulfilled,
      payload: userVariant,
    };
  },
  rejected() {
    return { type: types.fetchUserCurrentVariant.rejected };
  },
};

const fetchUserAccessories = {
  pending() {
    return { type: types.fetchUserAccessories.pending };
  },
  fulfilled(userAccessories) {
    return {
      type: types.fetchUserAccessories.fulfilled,
      payload: userAccessories,
    };
  },
  rejected() {
    return { type: types.fetchUserAccessories.rejected };
  },
};

const setUserInventory = (pets, accessories) => ({
  type: types.setUserInventory,
  payload: { pets, accessories },
});

const switchUserCurrentVariant = (updatedVariant) => ({
  type: types.switchUserCurrentVariant,
  payload: updatedVariant,
});

const purchaseAccessory = {
  pending() {
    return { type: types.purchaseAccessory.pending };
  },
  fulfilled() {
    return { type: types.purchaseAccessory.fulfilled };
  },
  rejected() {
    return { type: types.purchaseAccessory.rejected };
  },
};

const changeUserPetName = (newPetName) => ({
  type: types.changeUserPetName,
  payload: newPetName,
});

export {
  login,
  setUserBalance,
  fetchUserData,
  fetchUserProfile,
  logout,
  fetchUserCoins,
  fetchUserPets,
  fetchUserCurrentPet,
  fetchUserCurrentVariant,
  fetchUserAccessories,
  switchUserCurrentVariant,
  setUserInventory,
  purchaseAccessory,
  changeUserPetName,
  setUserId,
};
