const fetchAccessories = {
  pending: 'accessories/fetchAccessoriesPending',
  fulfilled: 'accessories/fetchAccessoriesFulfilled',
  rejected: 'accessories/fetchAccessoriesRejected',
};

export { fetchAccessories };
