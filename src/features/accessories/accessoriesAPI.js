import monola from '../../services/apis/monola';

export const fetchAccessories = async () => {
  const response = await monola.get('/accessories');

  return response.data;
};
