import * as types from './accessoriesTypes';
import { userTypes } from '../user';
import { IDLE, PENDING, FULFILLED, REJECTED } from '../../services/enums';

import { formatItemObjectData } from '../../services/helpers';

const INITIAL_STATE = {
  accessories: [],
  fetchAccessoriesStatus: IDLE,
  fetchAccessoriesError: false,
};

const accessoriesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.fetchAccessories.pending: {
      return {
        ...state,
        accessories: [],
        fetchAccessoriesStatus: PENDING,
        fetchAccessoriesError: false,
      };
    }

    case types.fetchAccessories.fulfilled: {
      const accessories = action.payload
        .map((accessory) =>
          formatItemObjectData(
            accessory.accessoryId,
            accessory.accessoryName,
            accessory.accessoryImage,
            accessory.accessoryPrice
          )
        )
        .filter((accessory) => accessory.itemImg);

      return { ...state, accessories, fetchAccessoriesStatus: FULFILLED };
    }

    case types.fetchAccessories.rejected: {
      return {
        ...state,
        fetchAccessoriesStatus: REJECTED,
        fetchAccessoriesError: true,
      };
    }

    case userTypes.logout: {
      return INITIAL_STATE;
    }

    default:
      return state;
  }
};

export default accessoriesReducer;
