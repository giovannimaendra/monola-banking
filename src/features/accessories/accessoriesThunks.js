import * as actions from './accessoriesActions';
import * as apis from './accessoriesAPI';

const fetchAccessories = () => async (dispatch) => {
  dispatch(actions.fetchAccessories.pending());

  try {
    const { content } = await apis.fetchAccessories();

    dispatch(actions.fetchAccessories.fulfilled(content));
  } catch (err) {
    dispatch(actions.fetchAccessories.rejected());
  }
};

export { fetchAccessories };
