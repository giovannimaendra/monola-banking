import monola from '../../services/apis/monola';

export const getRecipientByAccountNumber = async (accountNumber, bankName) => {
  const response = await monola.get('/user-profile/account-bank', {
    params: { accountNumber, bankName },
  });

  return response.data;
};

export const getRecipientById = async (recipientId) => {
  const response = await monola.get(
    `/user-profile/search-account/${recipientId}`
  );

  return response.data;
};

export const getUserFavoriteTransactions = async (userId) => {
  const response = await monola.get('/transaction-favorite', {
    params: { userId },
  });

  return response.data;
};

export const getRecipientByName = async (userFullName) => {
  const response = await monola.get('/user-profile/account', {
    params: { name: userFullName },
  });

  return response.data;
};

export const getUserLastTransactions = async (userId) => {
  const response = await monola.get('/core/transfers', {
    params: { id: userId },
  });

  return response.data;
};

export const postUserTransfer = async (infoObj) => {
  const response = await monola.post('/core/transfers', { ...infoObj });

  return response.data;
};

export const postFavoriteTransaction = async (infoObj) => {
  const response = await monola.post('/transaction-favorite', { ...infoObj });

  return response.data;
};
