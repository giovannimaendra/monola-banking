import * as types from './transferTypes';
import { userTypes } from '../user';

import { formatObjectData } from '../../services/helpers';

import { IDLE, PENDING, FULFILLED, REJECTED } from '../../services/enums';

const INITIAL_STATE = {
  recipient: null,
  favoriteTransactions: [],
  lastTransactions: [],
  transferAmount: 0,
  transferSchedule: '',
  transferDescription: '',
  transferIsFavorited: false,
  recipientDoesExists: false,
  recipientFetchStatus: IDLE,
  lastTransactionsFetchStatus: IDLE,
  favoriteTransactionsFetchStatus: IDLE,
  transferStatus: IDLE,
  transferErrorMessage: '',
  transferError: false,
  fetchRecipientError: false,
  fetchFavoriteTransactionsError: false,
  fetchLastTransactionsError: false,
};

const transferReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.fetchRecipientByAccount.pending: {
      return {
        ...state,
        recipientFetchStatus: PENDING,
        fetchRecipientError: false,
      };
    }

    case types.fetchRecipientByAccount.fulfilled: {
      const recipient = action.payload;
      return {
        ...state,
        recipient,
        recipientFetchStatus: FULFILLED,
      };
    }

    case types.fetchRecipientByAccount.rejected: {
      return {
        ...state,
        recipientFetchStatus: REJECTED,
        fetchRecipientError: true,
      };
    }

    case types.fetchRecipientById.pending: {
      return {
        ...state,
        recipient: null,
        recipientFetchStatus: PENDING,
        fetchRecipientError: false,
      };
    }

    case types.fetchRecipientById.fulfilled: {
      const { userId, userFullName, userAccountNumber, userBankName } =
        action.payload;
      const recipient = formatObjectData(
        userId,
        userFullName,
        userBankName,
        userAccountNumber
      );

      return { ...state, recipient, recipientFetchStatus: FULFILLED };
    }

    case types.fetchRecipientById.rejected: {
      return {
        ...state,
        recipientFetchStatus: REJECTED,
        fetchRecipientError: true,
      };
    }

    case types.fetchUserFavoriteTransactions.pending: {
      return {
        ...state,
        favoriteTransactionsFetchStatus: PENDING,
        favoriteTransactions: [],
        fetchFavoriteTransactionsError: false,
      };
    }

    case types.fetchUserFavoriteTransactions.fulfilled: {
      const favoriteTransactions = action.payload;
      return {
        ...state,
        favoriteTransactionsFetchStatus: FULFILLED,
        favoriteTransactions,
      };
    }

    case types.fetchUserFavoriteTransactions.rejected: {
      return {
        ...state,
        favoriteTransactionsFetchStatus: REJECTED,
        fetchFavoriteTransactionsError: true,
      };
    }

    // case types.setRecipientAsFavorite:
    //   const favoritedRecipient = action.payload;
    //   return {
    //     ...state,
    //     favoriteTransactions: [
    //       ...state.favoriteTransactions,
    //       favoritedRecipient,
    //     ],
    //   };

    case types.fetchUserLastTransactions.pending: {
      return {
        ...state,
        lastTransactionsFetchStatus: PENDING,
        lastTransactions: [],
        fetchLastTransactionsError: false,
      };
    }

    case types.fetchUserLastTransactions.fulfilled: {
      const lastTransactions = action.payload;
      return {
        ...state,
        lastTransactions,
        lastTransactionsFetchStatus: FULFILLED,
      };
    }

    case types.fetchUserLastTransactions.rejected: {
      return {
        ...state,
        lastTransactionsFetchStatus: REJECTED,
        fetchLastTransactionsError: true,
      };
    }

    case types.resetRecipient:
      return { ...state, recipient: null, recipientFetchStatus: IDLE };

    case types.setRecipient:
      const recipient = action.payload;
      return { ...state, recipient };

    case types.saveTransferInformation:
      const transferInfo = action.payload;
      return { ...state, ...transferInfo };

    case types.setIsFavorite:
      const isFavorite = action.payload;
      return { ...state, transferIsFavorited: isFavorite };

    case types.resetSavedInformations:
      return {
        ...state,
        transferAmount: 0,
        transferDescription: '',
        transferSchedule: '',
        recipientDoesExists: false,
      };
    case types.transferCash.pending:
      return {
        ...state,
        transferStatus: 'pending',
        transferError: false,
        transferErrorMessage: '',
      };
    case types.transferCash.fulfilled:
      return {
        ...state,
        transferStatus: 'fulfilled',
      };

    case types.transferCash.rejected: {
      const errorMsg = action.error;

      return {
        ...state,
        transferStatus: 'rejected',
        transferError: true,
        transferErrorMessage: errorMsg,
      };
    }
    case types.resetTransferStatus:
      return {
        ...state,
        transferStatus: 'idle',
        transferErrorMessage: '',
        transferError: false,
      };
    case types.setDoesExists:
      return {
        ...state,
        recipientDoesExists: action.payload,
      };

    case types.resetFetchLastTransactionsStatus:
      return {
        ...state,
        lastTransactionsFetchStatus: IDLE,
        lastTransactions: [],
      };

    case types.resetFetchFavoriteTransactionsStatus:
      return {
        ...state,
        favoriteTransactionsFetchStatus: IDLE,
        favoriteTransactions: [],
      };

    case userTypes.logout: {
      return INITIAL_STATE;
    }

    default:
      return state;
  }
};

export default transferReducer;
