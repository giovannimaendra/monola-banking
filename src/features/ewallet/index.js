import reducer from './eWalletReducer';

import * as eWalletTypes from './eWalletTypes';
import * as eWalletSelectors from './eWalletSelectors';
import * as eWalletActions from './eWalletActions';
import * as eWalletThunks from './eWalletThunks';

export { eWalletTypes, eWalletSelectors, eWalletActions, eWalletThunks };

export default reducer;
