import * as actions from './eWalletActions';
import { balanceActions } from '../balance';
import { userThunks } from '../user';

import { formatObjectData } from '../../services/helpers';

import * as apis from './eWalletAPI';

const fetchUserFavoriteEWallet = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  dispatch(actions.fetchUserFavoriteEWallet.pending());

  try {
    const { content } = await apis.getUserFavoriteEWallet(userId);

    const formattedData = content.map((ewallet) => {
      const data = formatObjectData(
        ewallet.id,
        ewallet.username,
        ewallet.provider,
        ewallet.phoneNumber
      );

      data.userId = ewallet.userId;

      return data;
    });

    dispatch(actions.fetchUserFavoriteEWallet.fulfilled(formattedData));
  } catch (err) {
    dispatch(actions.fetchUserFavoriteEWallet.rejected());
  }
};

const fetchFavoriteEWalletByName = (name) => async (dispatch) => {
  dispatch(actions.fetchFavoriteEWalletByName.pending());

  try {
    const { content } = await apis.getRecipientByName(name);

    const recipientData = {
      id: content[0].ewalletId,
      provider: content[0].ewalletProvider,
      identityNumber: content[0].ewalletPhoneNumber,
      userName: content[0].ewalletUsername,
    };

    dispatch(actions.fetchFavoriteEWalletByName.fulfilled(recipientData));
  } catch (err) {
    dispatch(actions.fetchFavoriteEWalletByName.rejected());
  }
};

const fetchEWalletByPhoneNumber =
  (phoneNumber, provider) => async (dispatch) => {
    dispatch(actions.fetchEWalletByPhoneNumber.pending());

    try {
      const { content } = await apis.getRecipientByPhoneNumber(
        phoneNumber,
        provider
      );

      const formattedData = formatObjectData(
        content[0].ewalletPaymentId,
        content[0].ewalletPaymentName,
        content[0].ewalletPaymentProvider,
        content[0].ewalletPaymentPhoneNumber
      );

      dispatch(actions.fetchEWalletByPhoneNumber.fulfilled(formattedData));
    } catch (err) {
      dispatch(actions.fetchEWalletByPhoneNumber.rejected());
    }
  };

const addEWalletToFavorite = (alias) => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  const recipientName = !!alias
    ? alias
    : getState().eWallet.eWalletRecipient.userName;
  const recipientPhoneNumber =
    getState().eWallet.eWalletRecipient.identityNumber;
  const recipientProvider = getState().eWallet.eWalletRecipient.provider;

  const recipientObj = {
    phoneNumber: recipientPhoneNumber,
    provider: recipientProvider,
    userId,
    username: recipientName,
  };

  try {
    await apis.addEWalletToFavorite(recipientObj);

    dispatch(
      actions.addEWalletToFavorite({ ...recipientObj, userName: recipientName })
    );
  } catch (err) {
    console.error(err.message);
  }
};

const topUpEWallet = (topUpObj) => async (dispatch) => {
  dispatch(actions.topUpEWallet.pending());

  try {
    await apis.topUpEWallet(topUpObj);

    dispatch(balanceActions.deductUserBalance(topUpObj.amount));
    dispatch(actions.topUpEWallet.fulfilled());
    dispatch(userThunks.fetchUserCoins());
  } catch (err) {
    const errorMsg = err.response.data.message;
    dispatch(actions.topUpEWallet.rejected(errorMsg));
  }
};

const changeFavoriteEWalletName =
  (updateObj, id) => async (dispatch, getState) => {
    const userId = getState().user.user.userId;

    updateObj.userId = userId;
    try {
      await apis.changeFavoriteEWalletName(updateObj, id);

      dispatch(
        actions.saveNewEwalletName(updateObj.username, updateObj.phoneNumber)
      );
    } catch (err) {
      console.error(err.message);
    }
  };

export {
  fetchUserFavoriteEWallet,
  fetchEWalletByPhoneNumber,
  fetchFavoriteEWalletByName,
  addEWalletToFavorite,
  topUpEWallet,
  changeFavoriteEWalletName,
};
