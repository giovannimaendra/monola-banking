import monola from '../../services/apis/monola';

export const getUserFavoriteEWallet = async (userId) => {
  const response = await monola.get('/eWallet-favorite-name/users', {
    params: { id: userId },
  });

  return response.data;
};

export const getRecipientByPhoneNumber = async (phoneNumber, provider) => {
  const response = await monola.get('/e-wallet-payments/e-wallets', {
    params: { phoneNumber, provider },
  });

  return response.data;
};

export const getRecipientByName = async (name) => {
  const response = await monola.get(`eWallet-favorite-name/name?name=${name}`);

  return response.data;
};

export const addEWalletToFavorite = async (recipientObj) => {
  const response = await monola.post('/eWallet-favorite-name', {
    ...recipientObj,
  });

  return response.data;
};

export const topUpEWallet = async (topUpObj) => {
  const response = await monola.post('/core/payments', { ...topUpObj });

  return response.data;
};

export const changeFavoriteEWalletName = async (updateObj, userId) => {
  const response = await monola.put(`/eWallet-favorite-name/${userId}`, {
    ...updateObj,
  });

  return response.data;
};
