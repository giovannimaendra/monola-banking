import * as types from './eWalletTypes';
import { userTypes } from '../user';

import { IDLE, PENDING, REJECTED, FULFILLED } from '../../services/enums';

const INITIAL_STATE = {
  eWalletRecipient: {},
  eWalletOnEdit: {},
  favoriteEWallets: [],
  topUpAmount: 0,
  topUpDescription: '',
  topUpErrorMessage: '',
  eWalletsFetchStatus: IDLE,
  eWalletSearchStatus: IDLE,
  topUpStatus: IDLE,
  eWalletsFetchError: false,
  eWalletSearchError: false,
  topUpError: false,
};

const eWalletReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    /**
     * Fetch Favorite eWallets
     */

    case types.fetchFavoriteEWallet.pending:
      return {
        ...state,
        eWalletsFetchStatus: PENDING,
        favoriteEWallets: [],
        eWalletsFetchError: false,
      };
    case types.fetchFavoriteEWallet.fulfilled:
      const favoriteEWallets = action.payload;
      return {
        ...state,
        eWalletsFetchStatus: FULFILLED,
        favoriteEWallets,
      };

    case types.fetchFavoriteEWallet.rejected:
      return {
        ...state,
        eWalletsFetchStatus: REJECTED,
        eWalletsFetchError: true,
      };

    /**
     * Fetch eWallet by Phone Number
     */

    case types.fetchEWalletByPhoneNumber.pending:
      return {
        ...state,
        eWalletSearchStatus: PENDING,
        eWalletSearchError: false,
        eWalletRecipient: {},
      };

    case types.fetchEWalletByPhoneNumber.fulfilled:
      const eWalletRecipient = action.payload;
      return {
        ...state,
        eWalletSearchStatus: FULFILLED,
        eWalletRecipient,
      };

    case types.fetchEWalletByPhoneNumber.rejected:
      return {
        ...state,
        eWalletSearchStatus: REJECTED,
        eWalletSearchError: true,
      };

    case types.fetchFavoriteEWalletByName.pending: {
      return {
        ...state,
        eWalletOnEdit: {},
        eWalletSearchStatus: PENDING,
        eWalletSearchError: false,
      };
    }

    case types.fetchFavoriteEWalletByName.fulfilled: {
      const ewallet = action.payload;

      return {
        ...state,
        eWalletOnEdit: ewallet,
        eWalletSearchStatus: FULFILLED,
      };
    }

    case types.fetchFavoriteEWalletByName.rejected: {
      return {
        ...state,
        eWalletSearchStatus: REJECTED,
        eWalletSearchError: true,
      };
    }

    /**
     * Misc
     */

    case types.resetEWalletRecipient:
      return { ...state, eWalletRecipient: {} };

    case types.resetFavoriteEWalletFetchStatus:
      return { ...state, eWalletsFetchStatus: IDLE };

    case types.addEWalletToFavorite.fulfilled:
      const newFavoriteEWallet = action.payload;
      return {
        ...state,
        favoriteEWallets: [...state.favoriteEWallets, newFavoriteEWallet],
      };

    case types.setEWalletRecipient: {
      const recipient = action.payload;
      return {
        ...state,
        eWalletRecipient: recipient,
      };
    }

    case types.setEditingEWallet: {
      const selectedEWallet = action.payload;
      return {
        ...state,
        eWalletOnEdit: selectedEWallet,
      };
    }

    case types.saveTopUpInfo:
      const topUpInfo = action.payload;
      return { ...state, ...topUpInfo };

    case types.resetSavedInfos:
      return { ...state, topUpAmount: 0, topUpDescription: '' };

    case types.saveNewEwalletName: {
      const { name, phoneNumber } = action.payload;
      const updatedFavoriteEwallet = state.favoriteEWallets.map((ewallet) => {
        if (ewallet.identityNumber === phoneNumber) {
          ewallet.userName = name;
        }

        return ewallet;
      });

      return { ...state, favoriteEWallets: updatedFavoriteEwallet };
    }

    /**
     * Top Up
     */

    case types.topUpEWallet.pending:
      return { ...state, topUpStatus: PENDING };

    case types.topUpEWallet.fulfilled:
      return { ...state, topUpStatus: FULFILLED };

    case types.topUpEWallet.rejected: {
      const errMsg = action.error;

      return {
        ...state,
        topUpStatus: REJECTED,
        topUpErrorMessage: errMsg,
        topUpError: true,
      };
    }

    case types.resetTopUpStatus:
      return {
        ...state,
        topUpStatus: IDLE,
        topUpErrorMessage: '',
        topUpError: false,
      };

    case types.resetEWalletRecipientSearchStatus: {
      return {
        ...state,
        eWalletSearchStatus: IDLE,
      };
    }

    case userTypes.logout: {
      return INITIAL_STATE;
    }

    default:
      return state;
  }
};

export default eWalletReducer;
