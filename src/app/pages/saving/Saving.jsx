import { useEffect, useState } from 'react';
import { Route, useRouteMatch, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Modal } from 'react-bootstrap';

import * as enums from '../../../services/enums';
import { savingSelectors, savingThunks } from '../../../features/saving';
import { userActions, userSelectors, userThunks } from '../../../features/user';

import { ReactComponent as Completed } from '../../../assets/img/Completed.svg';

import styles from './Saving.module.scss';

import { Button } from '../../../common/components/button';
import SavingHome from './SavingHome';
import AddNewSavingSteps from './AddNewSavingSteps';
import ManageSavingSteps from './ManageSavingSteps';
import Shop from '../shop';
import { DashboardLayout } from '../../../common/components/DashboardLayout';
import EditPet from '../editpet/EditPet';

const Saving = () => {
  const dispatch = useDispatch();
  const match = useRouteMatch();
  const history = useHistory();

  const [modalHeadingText, setModalHeadingText] = useState('');
  const [modalSmallText, setModalSmallText] = useState('');

  const [showModal, setShowModal] = useState(false);

  const userSavingsFetchingStatus = useSelector(
    savingSelectors.selectUserSavingsFetchingStatus
  );

  const userPets = useSelector(userSelectors.selectUserPets);
  const userAccessories = useSelector(userSelectors.selectUserAccessories);
  const userId = useSelector(userSelectors.selectUserId);

  const userPetsFetchingStatus = useSelector(
    userSelectors.selectUserPetsFetchingStatus
  );
  const userAccessoriesFetchingStatus = useSelector(
    userSelectors.selectUserAccessoriesFetchingStatus
  );

  const whichModalText = (manageType) => {
    if (manageType === 'topUp') {
      setModalHeadingText('Tambah Tabungan Berhasil');
      setModalSmallText('Saldo tabungan telah berhasil kamu tambah.');
    } else if (manageType === 'withdraw') {
      setModalHeadingText('Tarik Tabungan Berhasil');
      setModalSmallText('Saldo tabungan telah berhasil kamu tarik.');
    } else {
      setModalHeadingText('Buat Tabungan Baru Berhasil');
      setModalSmallText('Tabungan baru telah berhasil kamu tambah.');
    }
  };

  const handleModalOpen = (manageType) => {
    whichModalText(manageType);

    setShowModal(true);
  };

  const handleModalClose = () => {
    setShowModal(false);

    history.replace('/dashboard/saving');
  };

  useEffect(() => {
    (userSavingsFetchingStatus === enums.IDLE ||
      userSavingsFetchingStatus === enums.REJECTED) &&
      !!userId &&
      dispatch(savingThunks.fetchSaving());
  }, [dispatch, userSavingsFetchingStatus, userId]);

  useEffect(() => {
    userPetsFetchingStatus === enums.IDLE &&
      !!userId &&
      dispatch(userThunks.fetchUserPets());
  }, [userPetsFetchingStatus, dispatch, userId]);

  useEffect(() => {
    userAccessoriesFetchingStatus === enums.IDLE &&
      !!userId &&
      dispatch(userThunks.fetchUserAccessories());
  }, [userAccessoriesFetchingStatus, dispatch, userId]);

  useEffect(() => {
    userPetsFetchingStatus === enums.FULFILLED &&
      userAccessoriesFetchingStatus === enums.FULFILLED &&
      dispatch(userActions.setUserInventory(userPets, userAccessories));
  }, [
    userPetsFetchingStatus,
    userAccessoriesFetchingStatus,
    dispatch,
    userPets,
    userAccessories,
  ]);

  return (
    <>
      <DashboardLayout>
        <DashboardLayout.Head>
          <h3 className="dashboard-heading">Saving</h3>
        </DashboardLayout.Head>
        <DashboardLayout.Body>
          <Route exact path={`${match.path}`}>
            <SavingHome />
          </Route>
          <Route path={`${match.path}/add-new-saving`}>
            <AddNewSavingSteps onAddFinish={handleModalOpen} />
          </Route>
          <Route path={`${match.path}/manage-saving/:savingId`}>
            <ManageSavingSteps onManageFinish={handleModalOpen} />
          </Route>
          <Route path={`${match.path}/shop`}>
            <Shop />
          </Route>
          <Route path={`${match.path}/edit`}>
            <EditPet />
          </Route>
        </DashboardLayout.Body>
      </DashboardLayout>
      <Modal show={showModal} className="saving-modal">
        <Modal.Body className={styles.savingModalBody}>
          <Completed className={styles.completeIcon} />
          <div className="py-4 text-center">
            <p className={styles.modalHeadingText}>{modalHeadingText}</p>
            <p className={styles.modalSmallText}>{modalSmallText}</p>
          </div>
          <Button.Primary onClick={handleModalClose}>
            Kembali ke tabungan
          </Button.Primary>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Saving;
