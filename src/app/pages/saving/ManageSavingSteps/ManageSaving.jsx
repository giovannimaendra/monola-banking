import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import {
  separatorWithDot,
  getSavingIconComponent,
} from '../../../../services/helpers';

import { savingThunks, savingSelectors } from '../../../../features/saving';
import { userSelectors } from '../../../../features/user';

import styles from './ManageSaving.module.scss';

import { Button } from '../../../../common/components/button';
import { SavingContent } from '../SavingContent';
import ManageSavingLoader from './../../../../common/components/Loader/ManageSavingLoader';

const ManageSaving = ({ goToStep }) => {
  const { savingId } = useParams();

  const dispatch = useDispatch();

  const saving = useSelector(savingSelectors.selectManagedSaving);

  const managedSavingFetchingStatus = useSelector(
    savingSelectors.selectManagedSavingFetchingStatus
  );

  const userId = useSelector(userSelectors.selectUserId);

  useEffect(() => {
    // managedSavingFetchingStatus === 'idle' &&
    //   !checkObjectIsNotEmpty(saving) &&
    !!userId && dispatch(savingThunks.getAndSetManagedSaving(savingId));
  }, [dispatch, savingId, userId]);

  const amountSaving = separatorWithDot(saving.amount);

  const Icon = getSavingIconComponent(
    saving.goalIcon ? saving.goalIcon : 'Plane'
  );

  const handleTopUpClick = () => {
    // history.push(`${match.url}/manage?action=topup`);
    goToStep('topUp');
  };

  const handleWithdrawClick = () => {
    goToStep('withdraw');
  };

  return (
    <SavingContent>
      <SavingContent.Title>
        <h5 className="mb-0">Kelola Tabungan</h5>
      </SavingContent.Title>
      <SavingContent.Body className={styles.body}>
        {managedSavingFetchingStatus === 'pending' ? (
          <ManageSavingLoader />
        ) : (
          <>
            <div className={styles.containerImgIcon}>
              <Icon className={styles.savingIcon} />
            </div>
            <p className={styles.balance}>Rp {amountSaving}</p>
            <p className={styles.savingName}>{saving.goalName}</p>
            <p className={styles.description}>
              Bunga 2.5% p.a. dihitung harian dan dibayarkan setiap bulan
            </p>
            <div className="row mb-3">
              <div className="col px-2">
                <Button.Primary onClick={handleTopUpClick}>
                  Top Up
                </Button.Primary>
              </div>
              <div className="col px-2">
                <Button.Primary onClick={handleWithdrawClick}>
                  Tarik
                </Button.Primary>
              </div>
            </div>{' '}
          </>
        )}
      </SavingContent.Body>
    </SavingContent>
  );
};

export default ManageSaving;
