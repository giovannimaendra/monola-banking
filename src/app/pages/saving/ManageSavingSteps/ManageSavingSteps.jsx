import { useState } from 'react';
import { useDispatch } from 'react-redux';
import StepWizard from 'react-step-wizard';

import { savingThunks } from '../../../../features/saving';

import ManageSaving from './ManageSaving';
import ManageSavingInput from './ManageSavingInput';
import ManageSavingConfirm from './ManageSavingConfirm';

const ManageSavingSteps = ({ onManageFinish }) => {
  const dispatch = useDispatch();

  const [amount, setAmount] = useState(0);
  const [actionType, setActionType] = useState('');

  const handleAmountChange = (value) => {
    setAmount(value);
  };

  const handleActionType = (action) => {
    setActionType(action);
  };

  const handleActionSubmit = (cb) => {
    if (actionType === 'topUp') {
      dispatch(savingThunks.topUpSavingGoal(+amount));

      cb();

      return;
    }

    dispatch(savingThunks.withdrawSavingGoal(+amount));
    cb();
    return;
  };

  return (
    <StepWizard isHashEnabled isLazyMount>
      <ManageSaving hashKey="detail" />
      <ManageSavingInput
        hashKey="topUp"
        amount={amount}
        onChange={handleAmountChange}
        onClick={handleActionType}
      />
      <ManageSavingConfirm
        amount={amount}
        action={actionType}
        onSubmit={handleActionSubmit}
        onManageFinish={onManageFinish}
      />
      <ManageSavingInput
        hashKey="withdraw"
        amount={amount}
        onChange={handleAmountChange}
        onClick={handleActionType}
      />
      <ManageSavingConfirm
        amount={amount}
        action={actionType}
        onSubmit={handleActionSubmit}
        onManageFinish={onManageFinish}
      />
    </StepWizard>
  );
};

export default ManageSavingSteps;
