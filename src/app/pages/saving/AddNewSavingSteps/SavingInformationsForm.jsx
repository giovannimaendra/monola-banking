import { useState } from 'react';
import { Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
// import { useHistory } from 'react-router-dom';
import CurrencyInput from 'react-currency-input-field';

import styles from './SavingInformationsForm.module.scss';

import { savingThunks } from '../../../../features/saving';
import { balanceSelectors } from '../../../../features/balance';

import getSavingIconComponent from '../../../../services/helpers/getSavingIconComponent';

import ButtonSubmitSaving from '../../../../common/components/button/ButtonSubmitSaving';
import ChangeIconImageModal from '../../../../common/components/modal/ChangeIconImageModal';
import { SavingContent } from '../SavingContent';

import Camera from '../../../../assets/img/camera-icon.png';

const SavingInformationsForm = ({ previousStep, onAddFinish }) => {
  const dispatch = useDispatch();
  // const history = useHistory();

  const [modalShow, setModalShow] = useState(false);

  const [valueCurrency, setValueCurrency] = useState(0);
  const [selectedIcon, setSelectedIcon] = useState('Plane');
  const [currentSelectedIcon, setCurrentSelectedIcon] = useState('Plane');
  const [savingName, setSavingName] = useState('');
  const [savingNameLengthRemaining, setSavingNameLengthRemaining] =
    useState(30);
  const [errorMessage, setErrorMessage] = useState('');

  const userBalance = useSelector(balanceSelectors.selectUserBalance);

  const prefix = 'Rp. ';

  const handleOnClickEditIcon = () => {
    setModalShow(true);
  };

  const onHideModal = () => {
    setCurrentSelectedIcon(selectedIcon);
    setModalShow(false);
  };

  const handleOnValueChange = (value) => {
    setValueCurrency(value);
    if (value < 10000) {
      setErrorMessage('Minimal inputan saldo harus di atas Rp. 10.000');
    } else if (value > userBalance) {
      setErrorMessage('Nominal melebihi saldo utama anda');
    } else {
      setErrorMessage('');
    }
  };

  const handleOnSavingNameChange = (e) => {
    setSavingName(e.target.value);
    setSavingNameLengthRemaining(30 - e.target.value.length);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    dispatch(
      savingThunks.createNewSaving(savingName, valueCurrency, selectedIcon)
    );

    // history.replace('/dashboard/saving');
    onAddFinish();
  };

  const Icon = getSavingIconComponent(selectedIcon);

  const onClickIconItem = (goalIcon) => {
    setCurrentSelectedIcon(goalIcon);
  };

  const handleOnClickSimpan = (goalIcon) => {
    setSelectedIcon(goalIcon);
    setCurrentSelectedIcon(goalIcon);
    setModalShow(false);
  };

  const isDisabled = valueCurrency && savingName ? false : true;
  const errorMessageValidationBalance =
    valueCurrency > userBalance ? true : valueCurrency < 10000 ? true : false;

  return (
    <SavingContent>
      <SavingContent.Title onClick={previousStep}>
        <h5 className="mb-0">Tabungan Baru</h5>
      </SavingContent.Title>
      <SavingContent.Body>
        <div className="d-flex justify-content-center my-3">
          <div className="position-relative">
            <div className={styles.containerImgIcon}>
              <Icon className={styles.iconSaving} />
            </div>
            <img
              className={styles.cameraIcon}
              src={Camera}
              alt="Pick"
              onClick={handleOnClickEditIcon}
            />
          </div>
        </div>
        <Form className={styles.form}>
          <Form.Group controlId="nameSaving">
            <Form.Label className={styles.label}>Nama Tabungan</Form.Label>
            <Form.Control
              className={styles.input}
              type="text"
              placeholder="Tulis Nama Tabungan"
              maxLength="30"
              value={savingName}
              onChange={handleOnSavingNameChange}
              autoComplete="off"
            />
            <span className={styles.remaining}>
              {savingNameLengthRemaining}
            </span>
          </Form.Group>
          <Form.Group controlId="valueSaving">
            <Form.Label className={styles.label}>Nominal Saldo</Form.Label>
            <CurrencyInput
              className={styles.inputCurrency}
              id="valueSaving"
              placeholder="Masukkan Nominal"
              allowNegativeValue={false}
              groupSeparator="."
              maxLength="10"
              prefix={prefix}
              onValueChange={handleOnValueChange}
              autoComplete="off"
            />
            <span className={styles.errorMessage}>{errorMessage}</span>
          </Form.Group>
          <ButtonSubmitSaving
            disabled={isDisabled || errorMessageValidationBalance}
            onClick={handleSubmit}>
            Simpan
          </ButtonSubmitSaving>
        </Form>
      </SavingContent.Body>
      <ChangeIconImageModal
        show={modalShow}
        onHide={onHideModal}
        onClickIconItem={onClickIconItem}
        selectedIcon={currentSelectedIcon}
        onClickSimpan={handleOnClickSimpan}
      />
    </SavingContent>
  );
};

export default SavingInformationsForm;
