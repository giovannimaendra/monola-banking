import StepWizard from 'react-step-wizard';

import SavingPlans from './SavingPlans';
import SavingInformationsForm from './SavingInformationsForm';

const AddNewSavingSteps = ({ onAddFinish }) => {
  return (
    <StepWizard>
      <SavingPlans />
      <SavingInformationsForm onAddFinish={onAddFinish} />
    </StepWizard>
  );
};

export default AddNewSavingSteps;
