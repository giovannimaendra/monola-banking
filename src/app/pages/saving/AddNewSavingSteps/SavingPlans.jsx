import { classNameJoin } from '../../../../services/helpers';

import styles from './SavingPlans.module.scss';

import Piggy from '../../../../assets/img/piggy-bank.png';
import Monosave from '../../../../assets/img/monosave.png';

import ButtonTypeSaving from '../../../../common/components/button/ButtonTypeSaving';
import { SavingContent } from '../SavingContent';

const SavingPlans = ({ nextStep }) => {
  const monolaSavingClassName = classNameJoin(['row', styles.monoflex]);

  return (
    <SavingContent>
      <SavingContent.Title>
        <h5 className="mb-0">Buat tabungan baru</h5>
      </SavingContent.Title>
      <SavingContent.Body>
        <div className={monolaSavingClassName}>
          <div className="col px-4">
            <h6 className={styles.typeSaving}>Monoflex</h6>
            <p className={styles.subTypeSaving}>
              Jumlah alokasi tabungan dengan bunga 2.5% p.a.
            </p>
            <ButtonTypeSaving onClick={nextStep} />
          </div>
          <div className="col-4 d-flex flex-column justify-content-center">
            <img className={styles.imgSaving} src={Piggy} alt="monoflex" />
          </div>
        </div>
        <div className={monolaSavingClassName}>
          <div className="col px-4">
            <h6 className={styles.typeSaving}>Monosave</h6>
            <p className={styles.subTypeSaving}>
              Alokasi tabungan otomatis dengan bunga 2.5% p.a.
            </p>
            <ButtonTypeSaving />
          </div>
          <div className="col-4 d-flex flex-column justify-content-center">
            <img src={Monosave} alt="monosave" />
          </div>
        </div>
      </SavingContent.Body>
    </SavingContent>
  );
};

export default SavingPlans;
