import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import styles from './SavingContentItem.module.scss';

import { userSelectors } from '../../../../features/user';

import { classNameJoin } from '../../../../services/helpers';

import Coin from '../../../../common/components/coin/Coin';

import { ReactComponent as Eye } from '../../../../assets/img/eyeFilled.svg';

const SavingContentItem = ({ itemId, selector, onClick }) => {
  const location = useLocation();

  const item = useSelector((state) => selector(state, itemId));

  const isOwned = useSelector((state) =>
    userSelectors.selectUserItemInInventory(state, itemId)
  );

  const petShopItemClassName = classNameJoin([
    styles.petShopItem,
    !!isOwned ? styles.disabled : '',
  ]);

  const inventoryItemClassName = classNameJoin([
    styles.petShopItem,
    item.isChosen ? styles.disabled : '',
  ]);

  const whichPetShopClassName = location.pathname.includes('shop')
    ? petShopItemClassName
    : inventoryItemClassName;

  const whichValidation = location.pathname.includes('shop')
    ? isOwned
    : item.isChosen;

  const shouldRenderCoin = location.pathname.includes('shop') ? (
    <Coin coin={item.itemPrice} coinShop disabled={!!isOwned} />
  ) : (
    <div
      className={`${styles.coinContainer} ${item.isChosen && styles.disabled}`}>
      <Eye className={styles.coin} />
      <span className={styles.coinText}>Lihat</span>
    </div>
  );

  return (
    <div
      className="col-6 mt-3"
      onClick={whichValidation ? () => {} : () => onClick(itemId)}>
      <div className={whichPetShopClassName}>
        <div className={styles.petImgContainer}>
          <img src={item.itemImg} alt="pet-item" className={styles.itemImage} />
        </div>
        {shouldRenderCoin}
      </div>
    </div>
  );
};

export default SavingContentItem;
