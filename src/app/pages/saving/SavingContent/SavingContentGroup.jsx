import styles from './SavingContentGroup.module.scss';

import { classNameJoin } from '../../../../services/helpers';

const SavingContentGroup = ({ children }) => {
  const savingGroupClassName = classNameJoin([styles.savingGroup, 'row mt-3']);
  return <div className={savingGroupClassName}>{children}</div>;
};

export default SavingContentGroup;
