import styles from './SavingContentFooter.module.scss';

const SavingContentFooter = ({
  children,
  className: userClassName,
  styles: userStyles,
}) => {
  const footerClassName = userClassName ? userClassName : styles.savingFooter;

  return (
    <footer className={footerClassName} styles={userStyles ? userStyles : {}}>
      {children}
    </footer>
  );
};

export default SavingContentFooter;
