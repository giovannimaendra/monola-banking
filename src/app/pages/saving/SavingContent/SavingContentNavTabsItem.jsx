import styles from './SavingContentNavTabsItem.module.scss';

import { classNameJoin } from '../../../../services/helpers';

const SavingContentNavTabsItem = ({ children, onClick, active }) => {
  const buttonClassName = classNameJoin([
    styles.button,
    active && styles.active,
  ]);

  return (
    <div className="col">
      <button
        type="button"
        onClick={onClick ? onClick : () => {}}
        className={buttonClassName}>
        {children}
      </button>
    </div>
  );
};

export default SavingContentNavTabsItem;
