import { useState, useEffect } from 'react';
import StepWizard from 'react-step-wizard';
import { useDispatch, useSelector } from 'react-redux';
import { Modal } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

import {
  eWalletSelectors,
  eWalletThunks,
  eWalletActions,
} from '../../../features/ewallet';

import Panicman from '../../../assets/img/Panicman.png';

import styles from './EWalletSteps.module.scss';

import { Button } from '../../../common/components/button';
import FillEWalletInformations from './FillEWalletInformations';
import InsertPin from '../../../common/components/insertpin/InsertPin';
import TransactionResult from '../../../common/components/transactionresult/TransactionResult';
import TransactionSuccess from '../../../common/components/transactionsuccess/TransactionSuccess';
import ForgotPin from '../forgotpin/ForgotPin';

const EWalletSteps = ({
  recipient,
  userName,
  userBalance,
  userAccNum,
  onCoinReceived,
}) => {
  const history = useHistory();

  const [nominal, setNominal] = useState(0);
  const [description, setDescription] = useState('');
  const [pinInput, setPinInput] = useState('');
  const [isFavorited, setIsFavorited] = useState(false);
  const [attempt, setAttempt] = useState(0);

  const [showModal, setShowModal] = useState(false);

  const date = new Date().toLocaleString('id-ID', {
    day: '2-digit',
    month: 'long',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
  });

  const dispatch = useDispatch();

  const userId = useSelector((state) => state.user.user.userId);

  const savedTopUpNominal = useSelector((state) => state.eWallet.topUpAmount);

  const savedTopUpDescription = useSelector(
    (state) => state.eWallet.topUpDescription
  );

  useEffect(() => {
    if (attempt === 3) {
      setShowModal(true);
    }
  }, [attempt]);

  const handleTopUpSubmit = () => {
    const topUpObj = {
      accountName: recipient.userName,
      amount: +savedTopUpNominal,
      description: savedTopUpDescription,
      phoneNumber: recipient.identityNumber,
      pin: +pinInput,
      provider: recipient.provider,
      userId,
    };

    dispatch(eWalletThunks.topUpEWallet(topUpObj));
    setAttempt((prevState) => prevState + 1);

    if (isFavorited) {
      dispatch(eWalletThunks.addEWalletToFavorite());
    }
  };

  const handleNominalInput = (value) => {
    setNominal(value);
  };

  const handleDescriptionInput = (e) => {
    setDescription(e.target.value);
  };

  const handlePinChange = (value) => {
    setPinInput(value);
  };

  const handleFavoriteCheck = () => {
    setIsFavorited((prevState) => !prevState);
  };

  const handleModalClose = () => {
    setAttempt(0);
    setShowModal(false);

    dispatch(eWalletActions.resetTopUpStatus());

    history.replace('/dashboard');
  };

  return (
    <>
      <div>
        <StepWizard isHashEnabled isLazyMount>
          <FillEWalletInformations
            recipient={recipient}
            onNominalChange={handleNominalInput}
            onDescriptionChange={handleDescriptionInput}
            nominal={nominal}
            description={description}
            userName={userName}
            userBalance={userBalance}
            userAccNum={userAccNum}
          />
          <TransactionResult
            recipient={recipient}
            nominal={savedTopUpNominal}
            description={savedTopUpDescription}
            onCheck={handleFavoriteCheck}
            isFavorited={isFavorited}
          />
          <InsertPin
            recipient={recipient}
            pin={pinInput}
            onChange={handlePinChange}
            onClick={handleTopUpSubmit}
            attempt={attempt}
          />
          <ForgotPin />
          <TransactionSuccess
            recipient={recipient}
            date={date}
            nominal={savedTopUpNominal}
            description={savedTopUpDescription}
            selector={eWalletSelectors.selectEWalletRecipient}
            onCoinReceived={onCoinReceived}
          />
        </StepWizard>
      </div>
      <Modal
        show={showModal}
        onHide={handleModalClose}
        backdrop="static"
        className={styles.modal}>
        <Modal.Body className="d-flex flex-column justify-content-center px-5 pt-5">
          <img src={Panicman} alt="" className={styles.pinModalBody} />
          <p className={styles.paragraph}>
            Oh, tidak! Kamu telah memasukkan PIN yang salah berkali-kali. Untuk
            memulihkan PIN, segera hubungi Monola Help yang siap membantumu
            kapan saja melalui 1800-563.
          </p>
        </Modal.Body>
        <Button.Primary
          className={styles.backButton}
          onClick={handleModalClose}>
          Kembali ke beranda
        </Button.Primary>
      </Modal>
    </>
  );
};

export default EWalletSteps;
