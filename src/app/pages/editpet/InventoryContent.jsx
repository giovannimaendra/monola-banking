import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { SavingContent } from '../saving/SavingContent';

import { userSelectors, userThunks } from '../../../features/user';

import { useQuery } from '../../../common/hooks';

import styles from './InventoryContent.module.scss';

import ShopModal from '../../../common/components/modal/ShopModal';

const InventoryContent = () => {
  const [showModal, setShowModal] = useState(false);
  const [itemId, setItemId] = useState('');

  const item = useQuery().get('item');
  const dispatch = useDispatch();

  const userPets = useSelector(userSelectors.selectUserPets);
  const userAccessories = useSelector(userSelectors.selectUserAccessories);
  const userId = useSelector(userSelectors.selectUserId);

  const userCurrentPet = useSelector(userSelectors.selectUserCurrentPet);

  const petsFetchingStatus = useSelector(
    userSelectors.selectUserPetsFetchingStatus
  );
  const accessoriesFetchingStatus = useSelector(
    userSelectors.selectUserAccessoriesFetchingStatus
  );

  const handleCloseModal = () => {
    setItemId('');
    setShowModal(false);
  };

  const handleOpenModal = (value) => {
    setItemId(value);
    setShowModal(true);
  };

  const whichSelector =
    item === 'pet'
      ? userSelectors.selectUserPetById
      : userSelectors.selectUserAccessoryById;

  const usedData = item === 'pet' ? userPets : userAccessories;

  useEffect(() => {
    if (item === 'pet' && petsFetchingStatus === 'idle' && !!userId) {
      dispatch(userThunks.fetchUserPets());
    } else if (
      item === 'accessories' &&
      accessoriesFetchingStatus === 'idle' &&
      !!userId
    ) {
      dispatch(userThunks.fetchUserAccessories());
    }
  }, [item, dispatch, petsFetchingStatus, accessoriesFetchingStatus, userId]);

  const renderedItems = () => {
    if (
      item === 'accessories' &&
      userCurrentPet.petId !== '512004e2-5364-4f54-97ad-60ba4c330d2e'
    ) {
      return (
        <div className="py-2 mx-auto d-flex flex-column text-center mb-5">
          <h3 className={styles.emptyTitle}>Ups!</h3>
          <h3 className={styles.emptySubTitle}>
            Kamu belum mengkoleksi skin nih. Yuk beli skin dulu!
          </h3>
        </div>
      );
    } else if (item === 'house') {
      return (
        <div className="py-2 mx-auto d-flex flex-column text-center mb-5">
          <h3 className={styles.emptyTitle}>Ups!</h3>
          <h3 className={styles.emptySubTitle}>
            Kamu belum punya rumah nih. Yuk beli rumah dulu!
          </h3>
        </div>
      );
    }

    return usedData.map((item) => (
      <SavingContent.Item
        key={item.id}
        itemId={item.id}
        selector={whichSelector}
        onClick={handleOpenModal}
      />
    ));
  };

  return (
    <>
      <SavingContent.Group>{renderedItems()}</SavingContent.Group>
      {!!itemId && (
        <ShopModal
          show={showModal}
          onHide={handleCloseModal}
          selector={whichSelector}
          itemId={itemId}
        />
      )}
    </>
  );
};

export default InventoryContent;
