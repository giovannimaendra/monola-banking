import { NavLink, Route, useLocation } from 'react-router-dom';

import styles from './EditPet.module.scss';

import InventoryContent from './InventoryContent';
import { SavingContent } from '../saving/SavingContent';

const EditPet = () => {
  const location = useLocation();

  return (
    <SavingContent>
      <SavingContent.Title showCoin>
        <h5 className="mb-0">Edit</h5>
      </SavingContent.Title>
      <div className={styles.buttonContainer}>
        <NavLink
          to="/dashboard/saving/edit?item=pet"
          className={styles.navLink}
          activeClassName={
            location.search.includes('pet') ? styles.active : ''
          }>
          Hewan
        </NavLink>
        <NavLink
          to="/dashboard/saving/edit?item=accessories"
          className={styles.navLink}
          activeClassName={
            location.search.includes('accessories') ? styles.active : ''
          }>
          Aksesoris
        </NavLink>
        <NavLink
          to="/dashboard/saving/edit?item=house"
          className={styles.navLink}
          activeClassName={
            location.search.includes('house') ? styles.active : ''
          }>
          Rumah
        </NavLink>
      </div>
      <Route path="/dashboard/saving/edit">
        <InventoryContent />
      </Route>
    </SavingContent>
  );
};

export default EditPet;
