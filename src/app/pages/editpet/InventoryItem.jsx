import { useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { BiChevronRight } from 'react-icons/bi';

import { savingSelectors } from '../../../features/saving';

import styles from './SavingItem.module.scss';

import {
  classNameJoin,
  separatorWithDot,
  getSavingIconComponent,
} from '../../../services/helpers';

const InventoryItem = ({ savingId }) => {
  const history = useHistory();
  const location = useLocation();

  const saving = useSelector((state) =>
    savingSelectors.selectUserSavingsById(state, savingId)
  );

  const amountSaving = separatorWithDot(saving.amount);

  const Icon = getSavingIconComponent(saving.goalIcon);

  const savingItemClassName = classNameJoin(['row mt-3', styles.savingItem]);

  const handlingOnClick = () => {
    history.push(`${location.pathname}/manage-saving/${savingId}#detail`);
  };

  return (
    <div className={savingItemClassName} onClick={handlingOnClick}>
      <div className="col-2 justify-content-center d-flex">
        <Icon className={styles.icon} />
      </div>
      <div className="col">
        <p className={styles.savingName}>{saving.goalName}</p>
        <span className={styles.savingBalance}>Rp. {amountSaving}</span>
      </div>
      <div className="col-2 align-self-center">
        <BiChevronRight size="27px" />
      </div>
    </div>
  );
};

export default InventoryItem;
