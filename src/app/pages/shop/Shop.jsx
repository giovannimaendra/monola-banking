import { useLocation, Route, NavLink } from 'react-router-dom';

import styles from './Shop.module.scss';

import { SavingContent } from '../../../app/pages/saving/SavingContent';
import ShopContent from './ShopContent';

const Shop = () => {
  const location = useLocation();

  // const rowCN = classNameJoin(['row mt-3', styles.rowPetShop]);

  return (
    <SavingContent>
      <SavingContent.Title showCoin>
        <h5 className="mb-0">Shop</h5>
      </SavingContent.Title>
      <div className={styles.buttonContainer}>
        <NavLink
          to="/dashboard/saving/shop?item=pet"
          className={styles.navLink}
          activeClassName={
            location.search.includes('pet') ? styles.active : ''
          }>
          Hewan
        </NavLink>
        <NavLink
          to="/dashboard/saving/shop?item=accessories"
          className={styles.navLink}
          activeClassName={
            location.search.includes('accessories') ? styles.active : ''
          }>
          Aksesoris
        </NavLink>
        <NavLink
          to="/dashboard/saving/shop?item=house"
          className={styles.navLink}
          activeClassName={
            location.search.includes('house') ? styles.active : ''
          }>
          Rumah
        </NavLink>
      </div>
      <Route path="/dashboard/saving/shop">
        <ShopContent />
      </Route>
    </SavingContent>
  );
};

export default Shop;
