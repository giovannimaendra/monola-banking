import { Form, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

import styles from './CancelTransaction.module.scss';

const CancelTransaction = () => {
  const history = useHistory();

  const handleBackClick = () => {
    history.goBack();
  };

  const handleConfirmClick = () => {
    history.push('/dashboard');
  };

  return (
    <div className={styles.cancelContainer}>
      <div className={styles.content}>
        <h3 className={styles.heading}>Membatalkan Transaksi</h3>
        <p className={styles.paragraph}>
          Informasi transaksi yang sudah anda isi akan menghilang. Apakah Anda
          yakin ingin keluar dari halaman?
        </p>
        <Form.Group className="py-4 w-100">
          <div className="row justify-content-between">
            <div className="col-6">
              <Button
                variant="outline-primary"
                className={styles.cancelBtn}
                onClick={handleBackClick}>
                Batal
              </Button>
            </div>
            <div className="col-6">
              <Button
                className={styles.confirmBtn}
                onClick={handleConfirmClick}>
                Ok
              </Button>
            </div>
          </div>
        </Form.Group>
      </div>
    </div>
  );
};

export default CancelTransaction;
