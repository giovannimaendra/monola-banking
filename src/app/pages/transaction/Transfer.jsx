import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Form, InputGroup, FormControl } from 'react-bootstrap';
import { AiOutlineSearch } from 'react-icons/ai';
import { useHistory } from 'react-router-dom';

import styles from './Transfer.module.scss';

import * as enums from '../../../services/enums';

import {
  transferSelectors,
  transferThunks,
  transferActions,
} from '../../../features/transfer';

import { userSelectors } from '../../../features/user';
import { selectFilteredLastTransactions } from '../../../features/transfer/transferSelectors';

import TransactionGroup from '../../../common/components/transactionGroup/TransactionGroup';
// import CustomAlert from '../../../common/components/alert/CustomAlert';
import TransactionsLoader from '../../../common/components/Loader/TransactionsLoader';
import { EmptyFavorites } from '../../../common/components/EmptyFavorites';
import EmptyContent from '../../../common/components/EmptyContent/EmptyContent';

const Transfer = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const userId = useSelector(userSelectors.selectUserId);

  const fetchLastTransactionsStatus = useSelector(
    transferSelectors.selectLastTransactionsFetchStatus
  );

  const [accountInput, setAccountInput] = useState('');

  useEffect(() => {
    dispatch(transferActions.resetRecipient());
    fetchLastTransactionsStatus === enums.IDLE &&
      !!userId &&
      dispatch(transferThunks.fetchUserLastTransactions());
  }, [dispatch, fetchLastTransactionsStatus, userId]);

  const filteredLastTransactions = useSelector((state) =>
    selectFilteredLastTransactions(state, accountInput)
  );

  const handleAccountInput = (e) => {
    setAccountInput(e.target.value);
  };

  const handleItemClick = (recipientId) => {
    dispatch(transferThunks.fetchRecipientById(recipientId));

    history.push(
      `/dashboard/transaksi/transfer/fill-information?recipientId=${recipientId}`
    );
  };

  const renderTransactionItems = !!filteredLastTransactions.length ? (
    <>
      <h6 className="font-weight-bold">
        {!accountInput ? 'Transaksi Terakhir' : 'Hasil Pencarian'}
      </h6>
      <TransactionGroup
        items={filteredLastTransactions}
        onClick={handleItemClick}
        selector={transferSelectors.selectLastTransactionsById}
      />
    </>
  ) : !!accountInput && !filteredLastTransactions.length ? (
    // <CustomAlert>
    //   Akun tidak ditemukan. Mohon periksa kembali nama akun.
    // </CustomAlert>
    <EmptyContent />
  ) : fetchLastTransactionsStatus === 'pending' ? (
    <TransactionsLoader />
  ) : (
    <EmptyFavorites>
      <EmptyFavorites.Head>Transaksi Terakhir Masih Kosong</EmptyFavorites.Head>
      <EmptyFavorites.Paragraph>
        Lakukanlah transaksi baru dan apabila berhasil, maka akun terakhir
        penerima transfer akan ditambah disini.
      </EmptyFavorites.Paragraph>
    </EmptyFavorites>
  );

  return (
    <Form>
      <InputGroup className="mb-3">
        <FormControl
          placeholder="Cari Akun Bank"
          style={{ borderRight: 'none' }}
          onChange={handleAccountInput}
          value={accountInput}
          className={styles.searchInput}
        />
        <InputGroup.Append className={styles.append}>
          <InputGroup.Text className={styles.inputGroupText}>
            <AiOutlineSearch />
          </InputGroup.Text>
        </InputGroup.Append>
      </InputGroup>
      {renderTransactionItems}
    </Form>
  );
};

export default Transfer;
