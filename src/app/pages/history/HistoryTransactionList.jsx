import HistoryItem from './HistoryItem';

import styles from './HistoryTransactionList.module.scss';

const HistoryTransactionList = ({ transfers, date, onClickItem }) => {
  const renderedDataHistory = transfers.map((transfer) => (
    <HistoryItem
      key={transfer.id}
      itemId={transfer.id}
      name={transfer.accountName}
      bank={transfer.bankName}
      amount={transfer.amount}
      accountNumber={transfer.accountNumber}
      category={transfer.transactionCategoryName}
      dateDetail={transfer.date}
      onClick={onClickItem}
    />
  ));

  return (
    <div className="mt-4">
      <p className={styles.historyDate}>{date}</p>
      {renderedDataHistory}
    </div>
  );
};

export default HistoryTransactionList;
