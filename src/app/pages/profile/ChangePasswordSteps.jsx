import StepWizard from "react-step-wizard"

import ConfirmChangePassword from "./ConfirmChangePassword"
import FillPassword from "./FillPassword"
import SuccessChangePassword from "./SuccessChangePassword"
import Breadcrumb from "../../../common/components/BreadCrumb/Breadcrumb"

const ChangePasswordSteps = ({ 
    onClickBackPengaturan, 
    onChangeCurrentPassword, 
    onChangeNewPassword, 
    onChangeConfirmNewPassword,
    newPassword,
    currentPassword,
    confirmNewPassword,
    onClickBackLogin}) => {
  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item to="/dashboard/profile">
          Profil
        </Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item to="/dashboard/profile/pengaturan">Pengaturan</Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item>Ubah Password</Breadcrumb.Item>
    </Breadcrumb>
      <StepWizard isHashEnabled isLazyMount>
        <FillPassword 
          onClickBackPengaturan={onClickBackPengaturan}
          onChangeCurrentPassword={onChangeCurrentPassword}
          onChangeNewPassword={onChangeNewPassword}
          onChangeConfirmNewPassword={onChangeConfirmNewPassword}
          currentPassword={currentPassword}
          newPassword={newPassword}
          confirmNewPassword={confirmNewPassword} />
        <ConfirmChangePassword />
        <SuccessChangePassword onClickBackLogin={onClickBackLogin}/>
      </StepWizard>
    </div>
  )
}

export default ChangePasswordSteps