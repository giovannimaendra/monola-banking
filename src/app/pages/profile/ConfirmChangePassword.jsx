import { Button } from 'react-bootstrap';

import styles from './ConfirmChangePassword.module.scss';

const ConfirmChangePassword = ({ previousStep, nextStep }) => {
  return (
    <div className={styles.confirmContainer}>
      <p className={styles.title}>Ubah Password</p>
      <p className={styles.subTitle}>Password kamu akan diubah</p>
      <div className={styles.buttonContainer}>
        <Button className={styles.batalBtn} onClick={previousStep}>Batal</Button>
        <Button className={styles.okBtn} onClick={nextStep}>OK</Button>
      </div>
    </div>
  )
}

export default ConfirmChangePassword
