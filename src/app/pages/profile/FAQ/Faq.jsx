import FaqItem from "./FaqItem"

import { ReactComponent as TentangMonola } from '../../../../assets/img/tentangMonola.svg';
import { ReactComponent as MendaftarMonola } from '../../../../assets/img/mendaftarMonola.svg';
import { ReactComponent as MenggunakanMonola } from '../../../../assets/img/menggunakanMonola.svg';

import Breadcrumb from "../../../../common/components/BreadCrumb/Breadcrumb";

const Faq = ({ onClickTentangMonola }) => {
  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item to="/dashboard/profile">
          Profil
        </Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item>FAQ</Breadcrumb.Item>
    </Breadcrumb>
      <FaqItem Icon={TentangMonola} title="Tentang Monola" subTitle="Pertanyaan yang sering kami dengar ketika kamu baru mengenal Monola." onClick={onClickTentangMonola}/>
      <FaqItem Icon={MendaftarMonola} title="Mendaftar Monola" subTitle="Temukan pertanyaan buat kamu yang mau mulai bisa menggunakan Monola."/>
      <FaqItem Icon={MenggunakanMonola} title="Menggunakan Monola" subTitle="Pertanyaan seputar cara menggunakan Monola di keseharian kamu."/>
    </div>
  )
}

export default Faq
