import styles from './TentangMonolaContent.module.scss';

const TentangMonolaContent = ({ title, subTitle, points, text }) => {
  const renderedPoint = points && points.map((point) => (
        <li>{point}</li>))
  return (
    <div>
      <h6 className={styles.title}>{title}</h6>
      <p className={styles.subTitle}>{subTitle}</p>
      <p className={styles.text}>{text ? text : ''}</p>
      <ol className={styles.orderedList}>
        {points && renderedPoint}
      </ol>
    </div>
  )
}

export default TentangMonolaContent