import styles from './FaqItem.module.scss';

const FaqItem = ({ Icon, title, subTitle, onClick }) => {
  return (
    <div className={styles.faqItem} onClick={onClick}>
      <div className={styles.iconContainer}>
        <Icon />
      </div>
      <div className={styles.textContainer}>
        <h6 className={styles.title}>{title}</h6>
        <p className={styles.subTitle}>{subTitle}</p>
      </div>
    </div>
  )
}

export default FaqItem