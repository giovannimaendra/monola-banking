import styles from './Accordion.module.scss';

import { CgChevronDown, CgChevronRight } from 'react-icons/cg'

import { useState } from "react";

const Accordion = ({ content, title }) => {
  const [isActive, setIsActive] = useState(false);

  const onClickSwitch = () => {
    if (isActive === false) {
      setIsActive(true)
    } else  if (isActive === true) {
      setIsActive(false)
    }
  }

  return (
    <div className={styles.accordionItem} onClick={onClickSwitch}>
      <div className={styles.accordionTitle}>
        <h6 className={styles.title}>{title}</h6>
        <span className={styles.icon}>{isActive ? <CgChevronDown /> : <CgChevronRight />}</span>
      </div>
      {isActive && content}
    </div>
  )
}

export default Accordion
