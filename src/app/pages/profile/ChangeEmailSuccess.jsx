import { ReactComponent as Completed } from '../../../assets/img/Completed.svg';

import { Button } from 'react-bootstrap';

import styles from './ChangeEmailSuccess.module.scss';

const ChangeEmailSuccess = ({ onClickFinish }) => {
  return (
    <div className={styles.success}>
      <Completed />
      <p className={styles.title}>Email berhasil diubah!</p>
      <p className={styles.subTitle}>Mulai sekarang kamu sudah bisa menggunakan email barumu.</p>
      <Button className={styles.selesaiBtn} onClick={onClickFinish}>SELESAI</Button>
    </div>
  )
}

export default ChangeEmailSuccess
