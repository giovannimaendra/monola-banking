import styles from './ContactUs.module.scss';

import Breadcrumb from '../../../../common/components/BreadCrumb/Breadcrumb';

const ContactUs = () => {
  return (
    <>
    <Breadcrumb>
        <Breadcrumb.Item to="/dashboard/profile">
          Profil
        </Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item>Hubungi Kami</Breadcrumb.Item>
    </Breadcrumb>
    <div className={styles.contactUs}>
      <p className={styles.title}>Untuk informasi dan layanan produk Monola Indonesia serta penyampaian kritik, saran, dan keluhan, silakan menghubungi :</p>
      <ol className={styles.orderedList}>
        <li>Jika ingin menyampaikan Informasi, Saran atau Keluhan yang dapat memperbaiki kinerja kami dapat melalui email MonolaCare@monola.com</li>
        <li>Contact Center Monola Indonesia yang beroperasi 24 Jam melalui nomor telepon 14009</li>
      </ol>
    </div>
    </>
  )
}

export default ContactUs
