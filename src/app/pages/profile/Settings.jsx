import ListItemNavigation from "../../../common/components/listItemNavigation/ListItemNavigation"
import Breadcrumb from "../../../common/components/BreadCrumb/Breadcrumb";

const Settings = ({ onClickChangePassword, onClickFreezeCard, onClickUbahPin }) => {
  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item to="/dashboard/profile">
          Profil
        </Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item>Pengaturan</Breadcrumb.Item>
      </Breadcrumb>
      <ListItemNavigation onClick={onClickChangePassword} text='Ubah Password'/>
      <ListItemNavigation text='Ubah PIN' onClick={onClickUbahPin}/> 
      <ListItemNavigation onClick={onClickFreezeCard} text='Freeze Card'/> 
    </div>
  )
}

export default Settings
