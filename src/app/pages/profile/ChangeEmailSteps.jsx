import StepWizard from "react-step-wizard"
import { useState } from 'react';

import FillEmail from "./FillEmail"
import FillCodeVerification from "./FillCodeVerification";
import ChangeEmailSuccess from "./ChangeEmailSuccess";
import Breadcrumb from "../../../common/components/BreadCrumb/Breadcrumb";

const ChangeEmailSteps = ({ onClickToProfilSaya, onClickFinish }) => {
  const [email, setEmail] = useState('');

  const handleEmailChange = (input) => {
    setEmail(input)
  }

  console.log(email);

  return (
    <>
    <Breadcrumb>
        <Breadcrumb.Item to="/dashboard/profile">
          Profil
        </Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item to="/dashboard/profile/profil-saya">Profil Saya</Breadcrumb.Item>
    </Breadcrumb>
    <StepWizard isHashEnabled isLazyMount>
      <FillEmail email={email} onChangeEmail={handleEmailChange} onClickToProfilSaya={onClickToProfilSaya} />
      <FillCodeVerification email={email} />
      <ChangeEmailSuccess onClickFinish={onClickFinish}/>
    </StepWizard>
    </>
  )
}

export default ChangeEmailSteps
