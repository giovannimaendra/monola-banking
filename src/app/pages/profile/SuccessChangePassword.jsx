import { Button } from 'react-bootstrap';

import { ReactComponent as Completed } from '../../../assets/img/Completed.svg';

import styles from './SuccessChangePassword.module.scss';

const SuccessChangePassword = ({ onClickBackLogin }) => {
  return (
    <div className={styles.successChangePassword}>
      <Completed className={styles.completedIcon}/>
      <p className={styles.title}>Password berhasil diubah!</p>
      <p className={styles.subTitle}>Kamu sudah bisa login dengan password barumu</p>
      <div className={styles.buttonContainer}>
        <Button className={styles.kembaliLoginBtn} onClick={onClickBackLogin}>KEMBALI KE LOGIN</Button>
      </div>
    </div>
  )
}

export default SuccessChangePassword