import { Form, Button } from 'react-bootstrap';
import { useState } from 'react';

import styles from './FillEmail.module.scss';

import { classNameJoin, isValidEmail } from '../../../services/helpers';

const FillEmail = ({ email, onChangeEmail, onClickToProfilSaya, nextStep }) => {
  const [emailErrorMessage, setEmailErrorMessage] = useState('');

  const containerFillEmailCN = classNameJoin(['text-align-center d-flex mt-4 flex-column' , styles.containerFillEmail]);
  const inputClassName = classNameJoin([styles.input, !isValidEmail(email) && email ? styles.inputError : ''])

  const handleOnChangeEmail = (e) => {
    onChangeEmail(e.target.value)
    if (!isValidEmail(email)) {
      setEmailErrorMessage('Alamat email tidak valid')
    }
    else {
      setEmailErrorMessage('')
    }
  }

  return (
    <div className={styles.fillEmail}>
      <div className={containerFillEmailCN}>
        <span className={styles.title}>Pastikan email kamu aktif untuk keamanan akun<br/>dan memudahkan transaksimu</span>
        <Form>
          <Form.Group className={styles.formGroup}>
            <Form.Label className={styles.label}>Email</Form.Label>
            <Form.Control className={inputClassName} type="email" placeholder="Masukkan email" value={email} onChange={handleOnChangeEmail}/>
            <span className={styles.errorMessage}>{emailErrorMessage}</span>
          </Form.Group>
        </Form>
        <span className={styles.description}>Kode verifikasi akan dikirim ke email</span>
        <div className="d-flex justify-content-between">
          <Button className={styles.batalBtn} onClick={onClickToProfilSaya} >BATAL</Button>
          <Button className={styles.simpanBtn} disabled={!isValidEmail(email) ? true : false} onClick={nextStep}>SIMPAN</Button>
        </div>
      </div>
    </div>
  )
}

export default FillEmail
