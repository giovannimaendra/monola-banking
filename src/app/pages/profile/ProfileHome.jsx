import styles from './ProfileHome.module.scss';

import ListItemNavigation from '../../../common/components/listItemNavigation/ListItemNavigation';

const ProfileHome = ({ onClickProfil, onClickPengaturan, onClickHubungiKami, onClickFaq }) => {
  return (
    <div className={styles.profileHome}>
      <ListItemNavigation onClick={onClickProfil} text='Profil Saya'/>
      <ListItemNavigation onClick={onClickPengaturan} text='Pengaturan'/>
      <ListItemNavigation onClick={onClickFaq} text='FAQ'/>
      <ListItemNavigation onClick={onClickHubungiKami} text='Hubungi Saya'/>
    </div>
  )
}

export default ProfileHome
