import { useDispatch, useSelector } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';

import { userActions, userSelectors } from '../features/user';

import { localStorageHelpers } from '../services/helpers';

import { useScrollRestore } from '../common/hooks';

import BaseLayout from './BaseLayout';
import Dashboard from './pages/dashboard/Dashboard';
import LandingPage from './pages/landingpage/LandingPage';
import NotFound from './pages/notfound/NotFound';
import PrivacyPolicy from './pages/privacypolicy/PrivacyPolicy';
import TermsAndConditions from './pages/termsandconditions/TermsAndConditions';
import LoginPage from './pages/loginpage/LoginPage';

const App = () => {
  const userToken = localStorageHelpers.getUserToken();
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(userSelectors.selectIsLoggedIn);

  useScrollRestore();

  const handleUserLogout = () => {
    dispatch(userActions.logout());
  };

  return (
    <BaseLayout>
      <Switch>
        <Route exact path="/">
          {!!userToken && isLoggedIn ? (
            <Redirect to="/dashboard" />
          ) : (
            <LandingPage />
          )}
        </Route>
        <Route exact path="/privacy-policies">
          {!!userToken && isLoggedIn ? (
            <Redirect to="/dashboard" />
          ) : (
            <PrivacyPolicy />
          )}
        </Route>
        <Route exact path="/terms-and-conditions">
          {!!userToken && isLoggedIn ? (
            <Redirect to="/dashboard" />
          ) : (
            <TermsAndConditions />
          )}
        </Route>
        <Route path="/dashboard">
          {!userToken ? (
            <Redirect to="/" />
          ) : (
            <Dashboard handleUserLogout={handleUserLogout} />
          )}
        </Route>
        <Route path="/login">
          {!!userToken ? <Redirect to="/dashboard" /> : <LoginPage />}
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    </BaseLayout>
  );
};

export default App;
