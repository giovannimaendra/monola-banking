import scheduleReducer from './scheduleReducer';

import * as scheduleTypes from './scheduleTypes';
import * as scheduleActions from './scheduleActions';

export { scheduleTypes, scheduleActions };

export default scheduleReducer;
