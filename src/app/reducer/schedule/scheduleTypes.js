const SET_START_DATE = 'schedule/setStartDate';
const SET_END_DATE = 'schedule/setEndDate';
const SET_WHEN_DATE = 'schedule/setWhenDate';
const SET_DAY_OR_MONTH = 'schedule/setDayOrMonth';
const SET_SELECTED_DATE = 'schedule/setSelectedDate';
const SET_INTERVAL = 'schedule/setInterval';
const SET_SELECTED_DAY = 'schedule/setSelectedDay';
const RESET_SCHEDULE_STATE = 'schedule/resetScheduleState';

export {
  SET_START_DATE,
  SET_END_DATE,
  SET_WHEN_DATE,
  SET_DAY_OR_MONTH,
  SET_SELECTED_DATE,
  SET_INTERVAL,
  SET_SELECTED_DAY,
  RESET_SCHEDULE_STATE,
};
