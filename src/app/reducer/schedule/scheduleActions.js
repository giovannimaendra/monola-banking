import * as types from './scheduleTypes';

const setStartDate = (startDate) => ({
  type: types.SET_START_DATE,
  payload: startDate,
});

const setEndDate = (endDate) => ({
  type: types.SET_END_DATE,
  payload: endDate,
});

const setWhenDate = (whenDate) => ({
  type: types.SET_WHEN_DATE,
  payload: whenDate,
});

const setInterval = (interval) => ({
  type: types.SET_INTERVAL,
  payload: interval,
});

const setDayOrMonth = (dayOrMonth) => ({
  type: types.SET_DAY_OR_MONTH,
  payload: dayOrMonth,
});

const setSelectedDate = (selectedDate) => ({
  type: types.SET_SELECTED_DATE,
  payload: selectedDate,
});

const setSelectedDay = (selectedDay) => ({
  type: types.SET_SELECTED_DAY,
  payload: selectedDay,
});

const resetScheduleState = (initialState) => ({
  type: types.RESET_SCHEDULE_STATE,
  payload: initialState,
});

export {
  setStartDate,
  setEndDate,
  setWhenDate,
  setInterval,
  setDayOrMonth,
  setSelectedDate,
  setSelectedDay,
  resetScheduleState,
};
