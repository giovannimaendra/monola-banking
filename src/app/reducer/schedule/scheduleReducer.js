import * as types from './scheduleTypes';

const scheduleReducer = (state, action) => {
  switch (action.type) {
    case types.SET_START_DATE: {
      const startDate = action.payload;
      return { ...state, startDate };
    }

    case types.SET_END_DATE: {
      const endDate = action.payload;
      return { ...state, endDate };
    }

    case types.SET_WHEN_DATE: {
      const whenDate = action.payload;
      return { ...state, whenDate };
    }

    case types.SET_DAY_OR_MONTH: {
      const dayOrMonth = action.payload;
      return { ...state, dayOrMonth };
    }

    case types.SET_SELECTED_DATE: {
      const selectedDate = action.payload;
      return { ...state, selectedDate };
    }

    case types.SET_INTERVAL: {
      const interval = action.payload;
      return { ...state, interval };
    }

    case types.RESET_SCHEDULE_STATE: {
      return action.payload;
    }

    default: {
      return state;
    }
  }
};

export default scheduleReducer;
