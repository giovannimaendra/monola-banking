import * as types from './historyTypes';

const setHistoryData = (data) => ({
  type: types.SET_DATA_HISTORY,
  payload: data,
});

export { setHistoryData }