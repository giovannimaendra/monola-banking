# Dokumentasi FrontEnd Monola

Hello & welcome ke repo frontend tim Monola. Dokumentasi ini berisikan guide & detail-detail internal project untuk keperluan tim. Mohon dibaca dengan seksama, dan jadikan dokumentasi ini sebagai referensi.

*A well-refined developer always read the docs*.


# Getting Started
## Cloning Project

Lakukan command:

`git clone https://gitlab.com/binarxsynrgy-2_mainbootcamp/finalprojectsynrgy/team-b/frontend.git .`

Untuk cloning project pada folder lokal masing-masing. Apabila sudah, maka jalankan:

`code .`

Untuk membuka Visual Studio Code pada working directory tersebut.

## Dependencies Installation

Gunakan command:

`yarn`

Pada working directory untuk menginstall semua dependencies apabila sudah di clone.

**Untuk menjalankan server, jangan gunakan `yarn start`, karena sudah di reserve untuk keperluan deployment**. 

Gunakanlah:

`yarn dev`

Untuk memulai server seperti biasa.


## Tech Stacks

List libraries yang digunakan:
- React (Main frontend UI library)
- Bootstrap (CSS Framework)
- CSS Module (CSS as module import)
- React Router (SPA Routing)
- Redux (State Management library)
- Redux Thunk (Asynchronous Action handling middleware)
- React Testing Library (Unit & Integration testing library for react)
- React Icons (SVG wrapper as react components)


## Git

### Starting Out

Buatlah branch baru dari branch `development` dengan format sebagai berikut:

`git checkout -b feature/nama-feature`

Kemudian, lakukan development pada branch tersebut.

Untuk keperluan bug-fixing, branch-out dari `development` dengan format sebagai berikut:

`git checkout -b fixing/nama-hotfix`

**PENTING!!**

Jangan lupa untuk selalu melakukan 

`git checkout development` 

`git pull` 

sebelum melakukan `git push`. Apabila sudah melakukan `git pull`, kembali ke working branch 

`git checkout feature/nama-feature`, 

kemudian lakukan:

`git rebase -i --autosquash development`.

**Apabila terjadi conflict, silahkan resolve sendiri. Berhati-hatilah ketika resolve, karena dapat menyebabkan kehilangan ~~nyawa~~ code**.

Kalau terjadi conflict ketika `rebase`, resolve, kemudian lakukan:

`git add .`

`git rebase --continue`

Jikalau proses `rebase` sudah selesai, lakukan `push` ke repo

`git push -u origin feature/nama-feature -f`

Lakukan `Merge Request` dan notif ke saya apabila sudah siap. Akan saya cek untuk revisi.

***Kelalaian untuk melakukan proses-proses diatas dengan benar akan menyebabkan migraine satu tim***.

*Ketika berpindah branch atau melakukan `git pull`, lakukan `git stash` terlebih dahulu untuk menyimpan progress secara lokal.*

### Flow
Berikut merupakan step-by-step ketika melakukan development di local branch:

1. `git checkout -b feature/nama-feature`
2. *Lakukan perubahan*
3. `git add .`
4. `git commit -m "Fungsionalitas untuk transfer sudah berjalan sesuai harapan"`
5. `git checkout development`
6. `git pull`
7. `git checkout feature/nama-feature`
8.  `git rebase -i --autosquash development`
9.  **CONFLICT**
10. *Resolve conflict mandiri*
11.  `git add .`
12.  `git rebase --continue` 
13.  `git push -u origin feature/nama-feature`

**Dimohon untuk konsisten melakukan step-step di atas.**


## Project Structure

Berikut merupakan overall project structure yang akan diadopsi untuk final project:

```
my-app/ 
├─ node_modules/ 
├─ public/ 
│ ├─ favicon.ico 
│ ├─ index.html 
│ ├─ robots.txt 
├─ src/ 
| ├─ assets/ 							// Simpan media seperti image
│ │ ├─ img/
│ │ | ├─ iniFoto.jpg
│ │ ├─ scss/							// Main folder SCSS
│ │ | ├─ abstracts/						// Untuk reusability seperti variable
│ │ | | ├─ _variables.scss
│ │ | | ├─ _mixins.scss
│ │ | | ├─ _placeholders.scss
│ │ | | ├─ _index.scss					// index sebagai central @forward
│ │ | ├─ base/							// Untuk base styling
| | | | ├─ _utilities.scss				// Custom margin, padding ,dll
| | | | ├─ _typography.scss				// Berhubungan dengan text styling
| | | | ├─ _index.scss
│ │ | ├─ main.scss						// Central @forward untuk digunakan
│ ├─ app/ 								// App-wide setups dan layout/pages
│ │ ├─ pages/ 
│ │ | ├─ landingpage/ 					// Group file yang berhubungan
│ │ │ | ├─ LandingPage.jsx
│ │ │ | ├─ LandingPage.module.scss 
│ │ │ | ├─ LandingPage.test.js
│ │ │ ├─ dashboard/
│ │ │ | ├─ Dashboard.jsx
│ │ │ | ├─ Dashboard.module.scss 
│ │ │ | ├─ Dashboard.test.js
│ │ │ ├─ notfound/
│ │ │ | ├─ NotFound.jsx
│ │ │ | ├─ NotFound.module.scss 
│ │ ├─ App.jsx 							// Main entry point
│ │ ├─ App.test.js 
│ │ ├─ Layout.jsx 						// Generic layout
│ │ ├─ rootReducer.js 					// combineReducer
│ │ ├─ store.js 						// Redux store
│ ├─ common/ 
| | ├─ hooks/ 							// Custom hooks
| | | ├─ useHttp.js 
│ │ ├─ components/						// Generic reusable components
│ │ │ | ├─ button/ 
│ │ │ | | ├─ AlertButton.jsx 
│ │ │ | | ├─ AlertButton.module.scss 
│ │ │ | | ├─ Button.jsx 
│ │ │ | ├─ header/ 
│ │ │ | | ├─ Header.module.scss 
│ │ │ | | ├─ Header.jsx 
│ ├─ features/ 							// Main feature untuk project
│ │ ├─ transactions/ 
│ │ │ ├─ Transactions.jsx
│ │ │ ├─ transactionAPI.js 				// Kumpulan functions untuk request API
│ │ │ ├─ transactionsSlice.js 			// Reducer, action type, action creator
│ │ ├─ users/ 
│ │ │ ├─ usersSlice.js 
│ ├─ services/ 							// Untuk non-UI related
│ │ ├─ apis/ 							// Konfigurasi API
│ │ │ ├─ index.js 
│ │ │ ├─ transactions.js 
│ │ │ ├─ users.js 
│ │ ├─ helpers/ 						// Helper functions apabila diperlukan
│ │ │ ├─ localStorageHelper.js 
│ │ │ ├─ sortingHelper.js 
│ ├─ index.js 							// Mounting App ke HTML
│ ├─ index.scss 						// Global styling (seperti reset)
├─ .env.example 						// .env untuk informasi sensitif
├─ .gitignore 
├─ .gitlab-ci.yml 						// Setup deployment Heroku
├─ package.json 
├─ README.md 							// Dokumentasi
├─ setupTests.js
```

Struktur project di atas mengikuti rekomendasi dari dokumentasi `Redux` & `SASS`.

Key Term(s): **Reducks, 7-in-1 Architecture**

## Style Guide

### 1.  JSX

- Lakukan logika di luar `return` statement, usahakan `return` sebersih mungkin

	*Why:*
	> Tugas sebuah Component adalah untuk me-return JSX element(s), pemisahan logika dilakukan untuk memberikan perbedaan yang jelas dan juga kerapian code.

```jsx
	const Component = ({ list, isActive }) => {
		const renderedList = list.map(({ id, name })=> <li key={id}>{name}</li>;

		if(!isActive) {
			return <h2>There is no list</h2>;
		}
		
		return (
			<ul>{renderedList}</ul>
		);
	}
```
 - Usahakan untuk selalu menggunakan `object destructuring`
 
	 *Why:*
	 > Biar ga capek-capek mengetik referensi object terus seperti `ref.value1`, `ref.value2`, dst. Sederhanakan dengan `const { value1, value2 } = ref`.

- Perhatikan desain dari handout hi-fi UI/UX, dan tentukan secara mandiri struktur component yang baik dan benar (Apa yang menjadi Parent Component, State tinggal dimana, Component mana yang reusable, dll).

	*Why:*
	> Seorang developer react yang baik harus dapat memikirkan arsitektur yang rapi dan teratur untuk kebaikan diri sendiri dan orang lain/kolaborator ketika mereka membaca codenya.

- Pergunakan arrow function daripada normal function notation untuk functional component.

	*Why:*
	> Lebih modern.

- Hindari function definition secara inline pada event listener props jsx element.

	*Why:*
	> Jikalau component terlalu nested dan banyak children yang membutuhkan function tersebut, dapat terjadi redundant re-render (Component re-render disaat yang tidak diinginkan), sehingga mudah terkena bug.

	**Buruk!!**

	```jsx
		import { useState } from 'react';
		import ChildrenComponent from './ChildrenComponent';

		const Component = () => {
			const [state, setState] = useState(false);

			return (
				<ChildrenComponent onClick={() => setState(prevState => !prevState)} />
			);
		}
	```

	**Baik**

	```jsx
	import { useState } from 'react';
		import ChildrenComponent from './ChildrenComponent';

		const Component = () => {
			const [state, setState] = useState(false);

			const handleChildrenClick = () => {
				setState(prevState => !prevState);
			}


			return (
				<ChildrenComponent onClick={handleChildrenClick} />
			);
		}
	```

- Gunakan format penamaan `handleElementEvent` dan `onEvent` untuk event handler dan event listener prop

	*Why:*
	> Lebih jelas bahwa handler dan event listener itu untuk apa, sehingga menghindari migraine

	**Buruk!!**

	```jsx
		import { useState } from 'react';
		import ChildrenComponent from './ChildrenComponent';

		const Component = () => {
			const [state, setState] = useState(false);

			const clickHandler = () => {
				setState(prevState => !prevState);
			}


			return (
				<ChildrenComponent handleClick={clickHandler} />
			);
		}
	```

	**Baik**

	```jsx
	import { useState } from 'react';
		import ChildrenComponent from './ChildrenComponent';

		const Component = () => {
			const [state, setState] = useState(false);

			const handleChildrenClick = () => {
				setState(prevState => !prevState);
			}


			return (
				<ChildrenComponent onClick={handleChildrenClick} />
			);
		}
	```

- Apabila ada perubahan `state` yang bergantung pada `state` sebelumnya, jangan langsung memanipulasi `state` tersebut, gunakanlah `setState` dengan callback sebagai argument

	*Why:*
	> `setState` melakukan perubahan yang banyak sekaligus dengan cara "menumpuk" (batching) mereka. Tidak pasti bahwa `state` yang kita manipulasi itu merupakan `state` yang terbaru. Bisa saja kita mereferensikan ke `state` yang sudah usang.

	**Buruk!!**

	```jsx
	import { useState } from 'react';
		import ChildrenComponent from './ChildrenComponent';

		const Component = () => {
			const [state, setState] = useState(false);

			const handleChildrenClick = () => {
				setState(!state);
			}


			return (
				<ChildrenComponent onClick={handleChildrenClick} />
			);
		}
	```

	**Baik**

	```jsx
	import { useState } from 'react';
		import ChildrenComponent from './ChildrenComponent';

		const Component = () => {
			const [state, setState] = useState(false);

			const handleChildrenClick = () => {
				setState(prevState => !prevState);
			}


			return (
				<ChildrenComponent onClick={handleChildrenClick} />
			);
		}
	```

### 2. SCSS

- Selalu pergunakan format `import styles from './Component.module.scss'` ketika memberikan namespace ke module.
	
	*Why:*
	> Penggunaan **styles** sebagai namespace sudah menjadi convention di komunitas. Ada baiknya kita juga mengikuti mayoritas.

- Perhatikan baik-baik apabila ada value yang bermunculan terus, dan masukkan sebagai `variable` di `src/assets/scss/abstracts/_variables.scss`.

	*Why:*
	> Lebih mudah mereferensikan value yang sama daripada harus mengingat terus secara tepat angka yang digunakan, ketika ingin mengubah value sekaligus juga lebih mudah, tinggal ke variablenya dan mengubah satu value tersebut.

- Selalu pergunakan directive `@use '../../assets/scss/main.scss' as *;` ketika ingin menggunakan variables/mixins/placeholders daripada `@import`.

	*Why:*
	> `@import` akan dihapus nantinya dari SASS, dan juga `@import` memiliki scoping global, yang merupakan hal buruk karena rentan terjadi konflik apabila ada `class` dengan nama yang sama pada file lain. `@use` memiliki scoping lokal khusus hanya untuk file yang memanggilnya.

- Manfaatkan penamaan dan nesting yang baik pada file `.scss`
	
	*Why:*
	> Ngapain kita pake sass kalo ga manfaatin apa yang diberikan >:O

```jsx
	import styles from './Component.module.scss';
	
	const Component = () => {
		return (
			<div className={styles.component}>
				<div className={styles.componentChild}>
					<h2 className={styles.componentHeading}>Halo</h2>
				</div>
			</div>
		);
	}
```

```scss
@use '../../assets/scss/main.scss' as *;
.component {
	padding: 5rem;
	background-color: $color-primary-light;

	&:hover {
		background-color: lighten($color-primary-light, 20%);
	}

	&Child {
		margin: 10rem auto;
		color: $color-grey-light-1;
	}
	
	&Heading {
		font-size: $font-size-big;
		font-weight: $font-light;
	}
}
```

- Gunakan camelCase daripada kebab-case untuk penamaan class pada `.module.scss`

	*Why:*
	> Bakal ribet kalau pake kebab-case ketika mau menggunakan stylenya.

```jsx
import styles from './Component.module.scss';

const Component = () => {
	return (
		<div className={styles['.component']}>
			<h2 className={styles['.component-heading']}>Hai</h2>
		</div>
	);
}
```

- Pergunakan `@mixin` untuk style yang mirip

	*Why:*
	> DRY principle.

```scss
@use '../../main.scss' as *;

@mixin button($font-size) {
	font-size: $font-size;
	padding: 1rem 2rem;
	border: none;
	border-radius: 10rem;
	background-color: $color-primary;
	color: inherit;
	font-weight: $font-light;
	transition: .2s all;
	
	&:hover {
		transform: translateY(-1rem);
		box-shadow: $box-shadow-thin;
	}
}

.buttonLarge {
	@include button(2rem);
}
```

- Hindari penggunaan `.px`, gunakanlah relative unit (`.em`, `.rem`, `%`).

	*Why:*
	> Lebih mudah untuk mengatur responsiveness nantinya. `.em` relatif ke parent font-size, `.rem` relatif ke root font-size (16px default), `%` relatif ke parent width/height (jikalau digunakan untuk length).

### 3. Redux

*In Construction*

### 4. Testing

*In Construction*

## Closing Message

Tetap semangat dan fokus. We can do this.
